# Workflow for TCR Vaccinated data

This project consists of clustering 10X TCR data for vaccinated donors as well as a pre-vaccinated sample.
We were interested to see if the published public clone, characterized by CDRA3 with AA motif CA[G/A/V]XNYGGSQGNLIF


## Prepare data for five donor groups (25_28, 27, 4_17, 8_13_15 and prevac)

Start separate LSF jobs for each dataset:
```
for dataset in 25_28 27 4_17 8_13_15 prevac do;
  bsub -o prep_tcr_$dataset.log ./src/prepare_pseudo_seq.py -i inputs/tcr_vac_10x/$dataset -o public_db/tcrvac10x/$dataset --name $dataset -c AB --species human --threads 8
done
```

## Cluster prepared data

```
./src/cluster_db.py -i public_db/tcrvac10x/* -o public_db/tcrvac10x/cluster
```

## Extract metadata from prepared data and add to cluster results

```
./src/extract_cluster_results.py -i public_db/tcrvac10x/* -r public_db/tcrvac10x/cluster -o public_db/tcrvac10x/cluster/meta
```

## Compute chi-squared statistics for clusters

The clusters are sorted by sequence count of all members.
```
./src/cluster_meta_chisq_stats.py -i public_db/tcrvac10x/cluster/meta -g 25_28 27 4_17 8_13_15 prevac > tcr_clusters_seq_count
```

## Sort clusters by size and output pseudo seq of cluster rep

The clusters are sorted by member count, ignoring sequence counts.
```
./src/sort_cluster_meta.py -i public_db/tcrvac10x/cluster/meta/A/meta.tsv -g 25_28 27 4_17 8_13_15 prevac > tcr_clusters_member_count
```

## Merge alpha and beta clusters

This could also be run via LSF since it will take a while to complete (see above).
```
./src/merge_paired_cluster_results.py -i public_db/tcrvac10x/cluster/meta -o public_db/tcrvac10x/cluster/meta/paired.tsv
```

## Find TCR alpha sequences that match the desired motif

The CDR3 sequences do not contain anchor residues (C and F/W) so remove them from the pattern.
```
awk -F$'\t' '$10 ~ /TRA/ && $54 ~ /A[GAV].NYGGSQGNLI/ {print $1}' public_db/tcrvac10x/cluster/meta/paired.tsv | sort | uniq -c | sort -rn
```

## Write out a cluster of interest

```
./src/show_cluster.py -i public_db/tcrvac10x/cluster/meta/paired.tsv -c 4_17_0_210 > cluster_4_17_0_210
```

## Check out the beta chain gene frequency

```
grep TRB cluster_4_17_0_210 | awk '{print $5 "/" $6}' | sort | uniq -c | sort -nr
```
