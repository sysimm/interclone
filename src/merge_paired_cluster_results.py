#!/usr/bin/env python

# Merges expanded cluster results from two runs (heavy & light or alpha & beta) by a given column
# which identifies sequence pairs, e.g. clone_id. The output file is ordered so that the matched
# results should appear close together. Note that there aren't necessarily two entries per cell
# because either chain could have multiple hits. Entries are grouped by a new column "group_id".

import argparse
import gc
import logging
import sys
from collections import defaultdict
from pathlib import Path
import pandas as pd
from common import find_files_by_chain, ALLOWED_CHAINS, update_progress, write_progress


parser = argparse.ArgumentParser('merge paired search results')
parser.add_argument('-i', '--inputs', type=Path)
parser.add_argument('--heavy', '--beta', type=Path, help='heavy/beta result TSV')
parser.add_argument('--light', '--alpha', type=Path, help='light/alpha result TSV')
parser.add_argument('-c', '--chain', choices=ALLOWED_CHAINS,
                    help='chain(s) to process')
parser.add_argument('-o', '--output', type=Path, required=True, help='output TSV file')
parser.add_argument('--compare-by', '--by', default='clone_id', help='column name to identify pairs')
parser.add_argument('--member-col', default='Member', help='member ID column name')
parser.add_argument('--progress', type=Path, help='progress file path')
args = parser.parse_args()

progress_data = {
    'totalSteps': 1,
    'currentStep': 0,
    'status': 'running',
}
write_progress(progress_data, args.progress)

logging.basicConfig(level=logging.INFO)

if args.inputs:
    inputs = find_files_by_chain(args.inputs, args.chain, 'meta.tsv')
    if len(inputs) != 2:
        logging.error('provide one heavy/beta and one light/alpha input file')
        update_progress(args.progress, status='error')
        sys.exit(1)
    heavy_inputs, light_inputs = inputs.values()
    if len(heavy_inputs) != 1 or len(light_inputs) != 1:
        logging.error('provide one heavy/beta and one light/alpha input file')
        update_progress(args.progress, status='error')
        sys.exit(1)
    heavy_file = heavy_inputs[0]
    light_file = light_inputs[0]
else:
    if not args.heavy or not args.light:
        logging.error('provide either a folder via -i/--inputs or two files via --heavy/--beta and --light/--alpha')
        update_progress(args.progress, status='error')
        sys.exit(1)
    heavy_file = args.heavy
    light_file = args.light

# input validation
# when extracting metadata, columns may receive a '_query' suffix to prevent name clashes

heavy = pd.read_table(heavy_file, dtype=str)
if args.compare_by in heavy.columns:
    heavy_group_col = args.compare_by
elif args.compare_by + '_query' in heavy.columns:
    heavy_group_col = args.compare_by + '_query'
else:
    logging.error('comparison column %s not found in file %s', args.compare_by, args.heavy)
    update_progress(args.progress, status='error')
    sys.exit(1)

if args.member_col not in heavy.columns :
    logging.error('"%s"" column not found in file %s. Did you provide an expanded results file?', args.member_col, args.heavy)

light = pd.read_table(light_file, dtype=str)
if args.compare_by in light.columns:
    light_group_col = args.compare_by
elif args.compare_by + '_query' in light.columns:
    light_group_col = args.compare_by + '_query'
else:
    logging.error('comparison column %s not found in file %s', args.compare_by, args.light)
    update_progress(args.progress, status='error')
    sys.exit(1)

if args.member_col not in light.columns :
    logging.error('"%s"" column not found in file %s. Did you provide an expanded results file?', args.member_col, args.light)
    update_progress(args.progress, status='error')
    sys.exit(1)

try:
    # split up Query identifiers which are assumed to consist of three parts
    heavy[['query_name', 'query_file_idx', 'query_line_idx']] = heavy[args.member_col].str.rsplit('_', 2, expand=True)
    light[['query_name', 'query_file_idx', 'query_line_idx']] = light[args.member_col].str.rsplit('_', 2, expand=True)


    # Group the cluster results by query file because comparison values like clone_id
    # may only be unique per file.
    # data structure:
    # first level key - dataset name (e.g. 'DT') and file index (0, 1, etc)
    # second level key - 'heavy' or 'light'
    # values - set of clone_id values (or whatever column is used to find pairs)
    # {
    #     ('DT', 0): {
    #         'heavy': set('clone1', 'clone2', 'clone3'),
    #         'light': set('clone1', 'clone2')
    #     },
    #     ('DT', 1): {
    #         'heavy': set('clone1', 'clone47'),
    #         'light': set('clone1', 'clone2', 'clone47')
    #     }
    # }
    matches = defaultdict(dict)

    heavy_grouped = heavy.groupby(['query_name', 'query_file_idx'])
    for heavy_idx, heavy_group in heavy_grouped:
        matches[heavy_idx]['heavy'] = set(heavy_group[heavy_group_col])

    light_grouped = light.groupby(['query_name', 'query_file_idx'])
    for light_idx, light_group in light_grouped:
        matches[light_idx]['light'] = set(light_group[light_group_col])

    match_count = sum(1 for heavy_light_data in matches.values() if len(heavy_light_data) == 2)
    progress_data['totalSteps'] = match_count
    write_progress(progress_data, args.progress)

    # keep those results that share pair identifiers and discard the rest
    matched_data = []
    group_idx = 0
    for (query_name, query_file_idx), heavy_light_data in matches.items():
        # using the above example:
        # query_name = 'DT',
        # query_file_idx = 0,
        # heavy_light_data = {'heavy': set(...), 'light': set(...)}
        if len(heavy_light_data) < 2:
            continue

        update_progress(args.progress)
        common_entries = heavy_light_data['heavy'].intersection(heavy_light_data['light'])
        if not common_entries:
            # heavy and light don't have any clone_ids in common
            continue

        heavy_query = (heavy['query_file_idx'] == query_file_idx) & (heavy['query_name'] == query_name)
        light_query = (light['query_file_idx'] == query_file_idx) & (light['query_name'] == query_name)

        for group_id in sorted(common_entries):
            # Find those entries that satisfy all three requirements and assign them
            # a common group_id. Ideally, this would be one heavy and one light entry.
            heavy_mask = heavy_query & (heavy[heavy_group_col] == group_id)
            heavy.loc[heavy_mask, 'group_id'] = group_idx
            matched_data.append(heavy[heavy_mask])

            light_mask = light_query & (light[light_group_col] == group_id)
            light.loc[light_mask, 'group_id'] = group_idx
            matched_data.append(light[light_mask])

            group_idx += 1
            del heavy_mask, light_mask

        del heavy_query, light_query
        gc.collect()

    if matched_data:
        # write out matched (paired) data and ignore the rest
        merged = pd.concat(matched_data).convert_dtypes()
        merged.sort_values(['group_id', 'query_name', 'query_file_idx', 'query_line_idx']) \
            .to_csv(args.output, sep='\t', index=False)
        logging.info('wrote %s matches to %s', len(merged), args.output)
    else:
        logging.info('No matches found')

except Exception:
    logging.exception('Unable to merge cluster results')
    update_progress(args.progress, status='error')
else:
    update_progress(args.progress, status='finished')
