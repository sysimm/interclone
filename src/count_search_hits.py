#!/usr/bin/env python

# Prints out accumulated counts of unique sequences and clones based on search hits.

import argparse
import csv
import sys
from glob import iglob
from itertools import chain
from pathlib import Path
import pandas as pd

parser = argparse.ArgumentParser('extract search hits from input files')
parser.add_argument('-q', '--queries', nargs='+', type=Path, required=True,
                    help='folder(s) containing query AIRR files (should be same as in search call)')
parser.add_argument('-t', '--templates', nargs='+', type=Path, required=True,
                    help='folder(s) containing template AIRR files (should be same as in search call)')
parser.add_argument('-r', '--result', type=Path, required=True,
                    help='search result (hits) file path')
parser.add_argument('-d', '--detail', type=Path,
                    help='break down result by query and write to file (default: only show summary)')
args = parser.parse_args()

query_files = chain.from_iterable(iglob(f'{query_dir}/**/*.tsv', recursive=True) for query_dir in args.queries)
templ_files = chain.from_iterable(iglob(f'{templ_dir}/**/*.tsv', recursive=True) for templ_dir in args.templates)

query_parts = []
for query_file in query_files:
    query = pd.read_table(query_file, usecols=lambda col: col in ['sequence_id', 'sequence_aa', 'clone_count', 'author_sequence_id'], index_col='sequence_id')
    query['filename'] = str(query_file)
    if 'clone_count' not in query.columns:
        query['clone_count'] = 1
    query_parts.append(query)

queries = pd.concat(query_parts)

templ_parts = []
for templ_file in templ_files:
    templ = pd.read_table(templ_file, usecols=lambda col: col in ['sequence_id', 'sequence_aa', 'clone_count'], index_col='sequence_id')
    templ['filename'] = str(templ_file)
    if 'clone_count' not in templ.columns:
        templ['clone_count'] = 1
    templ_parts.append(templ)

templates = pd.concat(templ_parts)

results = pd.read_json(args.result) if args.result.suffix == '.json' else pd.read_table(args.result, usecols=['Query', 'Templ'])

if queries.empty:
    print('No query data found!')
    sys.exit()
elif templates.empty:
    print('No template data found!')
    sys.exit()
elif results.empty:
    print('No result data found!')
    sys.exit()

print(f'{len(results)} hits')
query_hits = queries.loc[results['Query'].unique()]

mask = queries['sequence_aa'].isin(query_hits['sequence_aa'])
query_grouped = queries.loc[mask].groupby(['sequence_aa', 'filename'])['clone_count']
query_seq_counts = query_grouped.size()
query_clone_counts = query_grouped.sum()
print(f'Query: {query_seq_counts.sum()} sequences, {query_clone_counts.sum()} clones')

templ_hits = templates.loc[results['Templ'].unique()]
mask = templates['sequence_aa'].isin(templ_hits['sequence_aa'])
templ_grouped = templates.loc[mask].groupby(['sequence_aa', 'filename'])['clone_count']
templ_seq_counts = templ_grouped.size()
templ_clone_counts = templ_grouped.sum()
print(f'Templ: {templ_seq_counts.sum()} sequences, {templ_clone_counts.sum()} clones')

if args.detail:
    with args.detail.open('w') as out_handle:
        detail_writer = csv.writer(out_handle, delimiter='\t', lineterminator='\n')
        detail_writer.writerow(['sequence_id', 'hits', 'seqs', 'clones'])
        for query_id, group in results.groupby('Query'):
            author_id = queries.loc[query_id, 'author_sequence_id']
            hit_count = len(group)
            # same as above
            templ_hits = templates.loc[group['Templ'].unique()]
            mask = templates['sequence_aa'].isin(templ_hits['sequence_aa'])
            templ_grouped = templates.loc[mask].groupby(['sequence_aa', 'filename'])['clone_count']
            templ_seq_counts = templ_grouped.size()
            templ_clone_counts = templ_grouped.sum()
            seq_count = templ_seq_counts.sum()
            clone_count = templ_clone_counts.sum()
            detail_writer.writerow([author_id, hit_count, seq_count, clone_count])
