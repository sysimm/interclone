#!/usr/bin/env python3

import os
import sys
import argparse
import shutil
import subprocess
import time
from pathlib import Path
import pandas as pd

anarci_exec = os.environ.get('ANARCI_BIN', shutil.which("ANARCI"))
if not os.path.isfile(anarci_exec):
    print('ERROR: ANARCI not found on host', os.uname()[1])
    sys.exit(1)

def test_for_file(cmd, path):
    if not os.path.isfile(path):
        print(" ".join(cmd))
        print("command failed...")
        sys.exit(1)


parser = argparse.ArgumentParser()
parser.add_argument("--input", required=True, type=Path, help="input fasta file")
parser.add_argument("--out", required=True, type=Path, help="aligned CDRs in fasta format")
parser.add_argument("--keep-gaps", action='store_true',help="keep gaps in CDRs")
parser.add_argument("--chain_type", default="H", choices=["H", "L", "B", "A"],
                    help="heavy, light, beta, alpha")
parser.add_argument("--species", choices=["human", "mouse", "rat", "rabbit", "rhesus", "pig", "alpaca", "cow", None],
                    help="annotate by specific species")
parser.add_argument("--ncpu", default="16", help="ncpu")
args = parser.parse_args()

fasta_file = args.input.resolve()
out_file = args.out.resolve()

if fasta_file.stat().st_size == 0:
    print('input file is empty')
    with out_file.open('w'):
        pass # create empty file to avoid later warning
    sys.exit()

chain_type = args.chain_type
scheme = "imgt"

out_dir = out_file.parent
out_dir.mkdir(parents=True, exist_ok=True)

"""
### IMGT: re_dict = {0 : "C...$", 2 : "^..W", 4 : "C$", 6 : "^[FW]"}	#G.G ###
CDR1: [27, 38]
CDR2: [56, 65]
CDR3: [105, 117]
FW4: [[3, 7], [20, 24], [39, 45], [49, 55], [76, 80], [87, 91], [100, 104], [118, 123]]

"""

#SGD-------LSYYN----GEEAS------------SS
#01234567890123456789012345678901234567
CDR_start = {
    1: "27",
    2: "56",
    3: "105",
}
CDR_end = {
    1: "38",
    2: "65",
    3: "117",
}

start = time.time()
anno_file = out_dir / (fasta_file.stem + "." + scheme + ".csv")
outfile = str(out_dir / (fasta_file.stem + "." + scheme))

chain_to_restrict = {
    "H": "heavy",
    "L": "light",
    "B": "B",
    "A": "tr", # ambiguous alpha/delta chain
}

annotate_cmd = [
    anarci_exec,
    "--sequence", str(fasta_file),
    "--scheme", scheme,
    "--restrict", chain_to_restrict[chain_type],
    "--outfile", outfile,
    "--ncpu", args.ncpu,
    "--csv"
]
if args.species:
    annotate_cmd += [
        "--use_species", args.species,
        "--assign_germline",
    ]

print(annotate_cmd)
subprocess.run(annotate_cmd, check=True)

anarci_end = time.time()
print('Anarci took %.3fs' % (anarci_end - start))

if chain_type == "H":
    suffix = "_H.csv"
    test_for_file(annotate_cmd,outfile + suffix)
    shutil.move(outfile + suffix, anno_file)

elif chain_type == "L":
    suffix = "_KL.csv"
    test_for_file(annotate_cmd,outfile + suffix)
    shutil.move(outfile + suffix, anno_file)

elif chain_type == "B":
    suffix = "_B.csv"
    test_for_file(annotate_cmd,outfile + suffix)
    shutil.move(outfile + suffix, anno_file)

elif chain_type == "A":
    # merge partial results
    if os.path.exists(outfile + "_A.csv"):
        anno_alpha = pd.read_csv(outfile + "_A.csv")
    else:
        anno_alpha = pd.DataFrame()

    if os.path.exists(outfile + "_D.csv"):
        anno_delta = pd.read_csv(outfile + "_D.csv")
    else:
        anno_delta = pd.DataFrame()

    # merge first to preserve column order
    anno_data = pd.concat([anno_alpha, anno_delta])

    # add missing MSA positions from alpha insertions
    missing_columns_delta = anno_alpha.columns.difference(anno_delta.columns, sort=False)
    if not anno_delta.empty and not missing_columns_delta.empty:
        anno_data[missing_columns_delta.tolist()] = '-'

    # do the reverse
    missing_columns_alpha = anno_delta.columns.difference(anno_alpha.columns, sort=False)
    if not anno_alpha.empty and not missing_columns_alpha.empty:
        anno_data[missing_columns_alpha.tolist()] = '-'

    if anno_data.empty:
        test_for_file(annotate_cmd, outfile) # assuming it was ANARCI's fault
    else:
        anno_data.to_csv(anno_file, index=False)

test_for_file("final check", anno_file)

entries = pd.read_csv(anno_file, index_col='Id')
# only use one result per input due ensure unique sequence IDs
entries_mask = entries['domain_no'] == 0
entries = entries.loc[entries_mask]
columns = entries.columns.tolist()

cdr_seqs = {}
for x in [1, 2, 3]:
    start_index = columns.index(CDR_start[x])
    end_index = columns.index(CDR_end[x]) + 1
    cdr_cols = columns[start_index:end_index]
    # extract CDR columns and combine them
    cdr_seqs[x] = entries[cdr_cols].apply(''.join, axis=1)

cdrs = cdr_seqs[1] + '_' + cdr_seqs[2] + '_' + cdr_seqs[3]
if args.keep_gaps:
    entries['pseudo_seq'] = cdrs
else:
    entries['pseudo_seq'] = cdrs.str.replace('-', '')

fasta = '>' + entries.index.astype(str) + '\n' + entries['pseudo_seq']

with open(out_file, 'w') as out_handle:
    out_handle.write(fasta.str.cat(sep='\n'))
    out_handle.write('\n')

end = time.time()
print(">>>  Time usage = %.3f[s]" % (end - start))
