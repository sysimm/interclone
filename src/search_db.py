#!/usr/bin/env python
# coding: utf-8

import argparse
import csv
import itertools
import json
import shutil
import subprocess
import sys
import tempfile

from functools import partial
from multiprocessing import Lock, Pool
from pathlib import Path

from common import find_files_by_chain, write_progress, update_progress
from common import ALLOWED_CHAINS, BASE_DIR, RUNNERS


lock = Lock() # used for local threads


def split_aln(key, aln):
    """ Split up aligned pseudo-sequences into individual CDRs.
        The CDR lengths are included at the end of the sequence ID,
        separated by underscores.
    """
    parts = key.rsplit("_", 3)

    name, len1, len2, len3 = parts

    cdr1_len = int(len1)
    cdr2_len = int(len2)
    cdr3_len = int(len3)

    cdrs = ["", "", ""]

    res_idx = 0
    for res in aln:
        if res_idx < cdr1_len:
            cdrs[0] += res
        elif res_idx < (cdr1_len + cdr2_len):
            cdrs[1] += res
        else:
            cdrs[2] += res

        if res != '-':
            res_idx += 1

    return name, cdrs


def calc_sid(a, b):
    """ Calculate sequence similarity as a percentage of identical residues """
    if not a or not b:
        return 0

    nmax = len(a)
    nsame = sum(a_res == b_res for a_res, b_res in zip(a, b))
    sid = 100 * nsame / nmax

    return int(sid)


def rescore_align(mmseqs_out, sid_cut1, sid_cut2, sid_cut3, min_cov):
    """ Calculate SID scores for each CDR for additional filtering.
        Since mmseqs is run with only a single SID cutoff, we now check if each
        CDR independently passes their respective similarity threshold. Only if
        that is the case will the search hit be included.
    """
    filtered = []

    with open(mmseqs_out, "r") as fin:
        for line in fin:
            qkey, tkey, fident, qcov, tcov, qaln, taln = line.strip().split("\t")

            if float(qcov) < min_cov or float(tcov) < min_cov:
                continue

            if int(100 * float(fident)) < sid_cut3:
                continue

            # split query and template aligned sequences
            qname, qaln123 = split_aln(qkey, qaln)
            tname, taln123 = split_aln(tkey, taln)

            sid1, sid2, sid3 = [calc_sid(qaln, taln) for qaln, taln in zip(qaln123, taln123)]

            if sid3 < sid_cut3 or sid2 < sid_cut2 or sid1 < sid_cut1:
                continue

            result = [qname, *qaln123, tname, *taln123, str(sid1), str(sid2), str(sid3)]
            filtered.append(result)

    return filtered


def search_db(query_db, target_db, result_db, align_out, min_sid, min_cov, temp_base):
    """ Execute the mmseqs commands to search prepared databases """
    tempdir = tempfile.mkdtemp(dir=temp_base)
    cmd = [
        "mmseqs", "search",
        str(query_db / "seq.db"),
        str(target_db / "seq.db"),
        str(result_db / "search_out"),
        str(tempdir),
        # "-s", "2",
        #        "--comp-bias-corr", "0",
        "--mask", "0",
        #        "--gap-open", "16",
        #        "--gap-extend", "2",
        #        "--min-length", "9",
        "-a",
        "--min-seq-id", str(min_sid),
        "-c", str(min_cov)
        #        "-v","0"
    ]

    try:
        result = subprocess.run(cmd, check=True, capture_output=True, text=True)
    except subprocess.CalledProcessError as ex:
        print(ex.stderr)
        print(' '.join(cmd))
        raise

    result_count = 0
    for line in result.stdout.splitlines():
        if line.startswith("Target database size:"):
            result_count += int(line.split()[3])

    cmd = [
        "mmseqs", "convertalis",
        str(query_db / "seq.db"),
        str(target_db / "seq.db"),
        str(result_db / "search_out"),
        str(align_out),
        "-v", "0",
        "--format-output", "query,target,fident,qcov,tcov,qaln,taln"
    ]

    subprocess.run(cmd, check=True, capture_output=True)

    shutil.rmtree(tempdir)

    return result_count


def run_search(out_folder, cov, sid1, sid2, sid3, progress_path, temp_base, query, templ, return_path=False):
    """ Entry point to search a query/template db combo
        Calls the necessary mmseqs commands and processes their results.
    """
    query_idx, query_db = query
    templ_idx, templ_db = templ
    query_name = query_db.name.removesuffix('-mmseqs')
    templ_name = templ_db.name.removesuffix('-mmseqs')

    dout_path = out_folder.resolve() / f'{query_idx}_{query_name}' / f'{templ_idx}_{templ_name}'
    dout_path.mkdir(parents=True)

    mmseqs_align_out = dout_path / "search_out.m8"
    min_sid = min(sid1, sid2, sid3) / 100
    min_cov = cov / 100
    if temp_base:
        temp_base = temp_base.resolve()

    nsearch = 0
    search_db_index = dout_path / "search_out.index"
    if not search_db_index.is_file():
        nsearch = search_db(query_db, templ_db, dout_path, mmseqs_align_out, min_sid, min_cov, temp_base)

    result = rescore_align(mmseqs_align_out, sid1, sid2, sid3, min_cov)

    with lock:
        update_progress(progress_path, nsearch)

    if return_path:
        return result, dout_path
    else:
        return result


def run_lsf(chain_out, database_pairs, progress_path, args):
    """ Set up Sysimm Runner inputs for execution with LSF, Slurm, etc.
        Each job handles one query/template combo and writes out a result
        TSV. These are then read in and combined.
    """
    lsf_template = BASE_DIR / 'src' / 'search_db_lsf.py'
    lsf_queries = chain_out / 'lsf_in_query'
    lsf_templates = chain_out / 'lsf_in_template'
    lsf_query_ids = chain_out / 'lsf_in_query_id'
    lsf_template_ids = chain_out / 'lsf_in_template_id'
    lsf_logs = chain_out / 'lsf_logs'
    with open(lsf_queries, 'wt') as queries_handle, \
         open(lsf_templates, 'wt') as templates_handle, \
         open(lsf_query_ids, 'wt') as query_ids_handle, \
         open(lsf_template_ids, 'wt') as template_ids_handle, \
         open(lsf_logs, 'wt') as logs_handle:

        for query, templ in database_pairs:
            query_id, query_dir = query
            templ_id, templ_dir = templ

            queries_handle.write(f'{query_dir}\n')
            templates_handle.write(f'{templ_dir}\n')
            query_ids_handle.write(f'{query_id}\n')
            template_ids_handle.write(f'{templ_id}\n')

            query_name = query_dir.name.removesuffix('-mmseqs')
            templ_name = templ_dir.name.removesuffix('-mmseqs')
            log_file = chain_out / f'{query_id}_{query_name}_{templ_id}_{templ_name}.log'
            logs_handle.write(f'{log_file}\n')

    lsf_cmd = [
        RUNNERS[args.parallel],
        '-template', lsf_template,
        '-jobs', str(args.threads),
        '-args', BASE_DIR, chain_out, str(args.sid1), str(args.sid2), str(args.sid3),
                 str(args.cov), progress_path, lsf_queries, lsf_templates, lsf_query_ids,
                 lsf_template_ids, lsf_logs
    ]
    subprocess.run(lsf_cmd, check=True)

    # accumulate job outputs
    output_data = []
    lsf_results = chain_out.glob('**/searchhits.tsv')
    for partial_result in lsf_results:
        with open(partial_result, newline='') as result_handle:
            csv_reader = csv.reader(result_handle, delimiter='\t')
            output_data.extend(csv_reader)

    return output_data


def write_output(output_folder, output_data, output_format, progress_path):
    header = [
        "Query", "QCDR1", "QCDR2", "QCDR3",
        "Templ", "TCDR1", "TCDR2", "TCDR3",
        "CDR1_Ident", "CDR2_Ident", "CDR3_Ident"
    ]

    filename_ext = '.json' if output_format == "json" else '.tsv'
    filename = 'searchhits' + filename_ext
    output_file = output_folder / filename
    with open(output_file, 'w') as out_handle:
        if output_format == "wide":
            out_handle.write("\t".join(header) + "\n")
            for dat in output_data:
                out_handle.write("\t".join(dat) + "\n")

        elif output_format == "paired":
            for dat in output_data:
                out_handle.write("SID\t" + "\t".join(dat[8:11]) + "\n")
                out_handle.write("\t".join(dat[0:4]) + "\n")
                out_handle.write("\t".join(dat[4:8]) + "\n")
        elif output_format == "json":
            json_data = []
            for dat in output_data:
                json_data.append(dict(zip(header, dat)))
            out_handle.write(json.dumps(json_data))

    print('wrote outputs to', str(output_file))
    update_progress(progress_path)


def main():
    parser = argparse.ArgumentParser(description='query a single mmseqs db against another')
    parser.add_argument('-q', dest='query_db', nargs='+', type=Path, required=True,
                        help='a folder containing one or more query mmseqs databases')
    parser.add_argument('-t', dest='templ_db', nargs='+', type=Path, required=True,
                        help='a folder containing one or more template mmseqs databases')
    parser.add_argument('-o', dest='out_folder', type=Path, required=True,
                        help='output_folder')
    parser.add_argument('--sid1', dest='sid1', type=int, default=90,
                        help='cdr1 % sequence identity cutoff [0-100]')
    parser.add_argument('--sid2', dest='sid2', type=int, default=90,
                        help='cdr2 % sequence identity cutoff [0-100]')
    parser.add_argument('--sid3', dest='sid3', type=int, default=80,
                        help='cdr3 % sequence identity cutoff [0-100]')
    parser.add_argument('--cov', dest='cov', type=int, default=90,
                        help='min % align coverage [0-100]')
    parser.add_argument('--chain', choices=ALLOWED_CHAINS,
                        help='chain to use for searching (default: determine automatically)')
    parser.add_argument('--parallel', choices=['local', 'lsf', 'slurm'],
                        help='method of parallelization (default: none)')
    parser.add_argument('--threads', '--jobs', type=int, default=8,
                        help='number of parallel threads/jobs')
    parser.add_argument('--temp-dir', type=Path,
                        help='base folder for temporary data')
    parser.add_argument('--clean', dest='clean', action="store_true",
                        help='remove output folder if it exists')
    parser.add_argument('--format', choices=["wide", "paired","json"], default="wide",
                        help='flag to control fomatting')

    args = parser.parse_args()

    # check if parallelization method works
    if args.parallel == 'lsf' and not shutil.which('bsub'):
        print('ERROR: Unable to use LSF, please choose another parallelization option')
        sys.exit(1)
    elif args.parallel == 'slurm' and not shutil.which('sbatch'):
        print('ERROR: Unable to use Slurm, please choose another parallelization option')
        sys.exit(1)

    dout = args.out_folder

    if args.clean and dout.is_dir():
        shutil.rmtree(dout)

    dout.mkdir(parents=True, exist_ok=True)

    #initialize progress with minimum data length
    progress_data = {
        'totalSteps': 2,
        'currentStep': 0,
        'numSeq': 0,
        'status': 'pending', # for error reporting
    }
    progress_path = dout / "progress.json"
    write_progress(progress_data, progress_path)

    query_folders = find_files_by_chain(args.query_db, args.chain, '*-mmseqs')
    if not query_folders:
        print('No suitable query databases found')
        update_progress(progress_path, status='error')
        sys.exit(1)

    templ_folders = find_files_by_chain(args.templ_db, args.chain, '*-mmseqs')
    if not templ_folders:
        print('No suitable template databases found')
        update_progress(progress_path, status='error')
        sys.exit(1)

    if query_folders.keys() != templ_folders.keys():
        print('Query and template database chains are mismatched', query_folders.keys(), templ_folders.keys())
        update_progress(progress_path, status='error')
        sys.exit(1)

    if args.chain and set(query_folders) != set(args.chain):
        print(f'Could not find databases for all requested chains. Found {set(query_folders)}')

    chains = query_folders.keys()
    print('using chains', ''.join(chains))

    search_steps = sum(len(query_folders[chain]) * len(templ_folders[chain]) for chain in chains)

    #initialize progress with actual data length
    extra_steps = len(chains) # for writing outputs
    progress_data['totalSteps'] = search_steps + extra_steps
    progress_data['status'] = 'running'
    write_progress(progress_data, progress_path)

    if args.temp_dir:
        args.temp_dir.mkdir(exist_ok=True, parents=True)

    try:
        for chain in chains:
            queries = query_folders[chain]
            templates = templ_folders[chain]
            database_pairs = itertools.product(enumerate(queries), enumerate(templates))
            chain_out = dout / chain
            chain_out.mkdir()

            if not args.parallel:
                output_data = []
                for query, templ in database_pairs:
                    result = run_search(chain_out, args.cov, args.sid1, args.sid2, args.sid3,
                                        progress_path, args.temp_dir, query, templ)
                    output_data.extend(result)

            elif args.parallel == 'local':
                partial_process = partial(run_search, chain_out, args.cov, args.sid1, args.sid2,
                                          args.sid3, progress_path, args.temp_dir)
                pool = Pool(args.threads)
                results = pool.starmap(partial_process, database_pairs)
                # flatten results
                output_data = list(itertools.chain.from_iterable(results))

            elif args.parallel in ('lsf', 'slurm'):
                output_data = run_lsf(chain_out, database_pairs, progress_path, args)

            write_output(chain_out, output_data, args.format, progress_path)

        update_progress(progress_path, status='finished')

    except Exception as ex:
        update_progress(progress_path, status='error')
        raise Exception('Unable to perform search') from ex


if __name__ == '__main__':
    main()
