#!/usr/bin/env python

import os
import subprocess
from pathlib import Path
import numpy as np
import pandas as pd

WORK = Path('/mounts/scratch04/jwila/cdr_cluster/work/')
# OUTDIR = WORK / 'compairr/search/covid'
OUTDIR = WORK / 'compairr/search/healthy'
GENE_DIR = OUTDIR / 'anarci'
ENHAB_INPUTS = OUTDIR / 'enhancing_antibodies.tsv'
# TARGET_INPUTS = OUTDIR / 'Kim-2021.tsv'
TARGET_INPUTS = OUTDIR / 'Healthy.tsv'
INTERCLONE_RESULT = OUTDIR / 'interclone_hits.tsv'
# INTERCLONE_RESULT = Path('/mounts/scratch04/jwila/cdr_cluster/work/search_enhab_kim/H/searchhits.tsv')
# INTERCLONE_RESULT = Path('/mounts/scratch04/jwila/cdr_cluster/work/search_enhab_kim-medium/H/searchhits.tsv')
# INTERCLONE_RESULT = Path('/mounts/scratch04/jwila/cdr_cluster/work/search_enhab_kim-low/H/searchhits.tsv')
# INTERCLONE_RESULT = Path('/mounts/scratch04/jwila/cdr_cluster/work/search_enhab_ggm-medium/H/searchhits.tsv')
# INTERCLONE_RESULT = Path('/mounts/scratch04/jwila/cdr_cluster/work/search_enhab_ggm/H/searchhits.tsv')

# TODO: use expanded search results to get duplication count

if not ENHAB_INPUTS.is_file():
    print('creating', ENHAB_INPUTS)
    enhab = pd.read_table(WORK / 'enhab/H/known_enhancing_ab.tsv', usecols=['sequence_id', 'sequence_aa', 'acdr3'])
    enhab['junction_aa'] = np.fromiter((full_seq[full_seq.find(cdr3)-1:full_seq.find(cdr3)+len(cdr3)+1] for full_seq, cdr3 in zip(enhab['sequence_aa'].values.astype(str), enhab['acdr3'].values.astype(str))), 'S99', len(enhab)).astype(str)
    enhab['repertoire_id'] = 'enhab'

    enhab_genes = pd.read_csv(GENE_DIR / 'enhab_human_H.csv', usecols=['Id', 'v_gene', 'j_gene'])
    enhab_genes[['sequence_id', 'foo']] = enhab_genes['Id'].str.split(' ', expand=True)

    enhab.merge(enhab_genes, left_on='sequence_id', right_on='sequence_id') \
        .rename(columns={'v_gene': 'v_call', 'j_gene': 'j_call'}) \
        .to_csv(ENHAB_INPUTS, sep='\t', index=False, columns=['repertoire_id', 'sequence_id', 'junction_aa', 'v_call', 'j_call'])

if not TARGET_INPUTS.is_file():
    print('creating', TARGET_INPUTS)
    target_parts = []
    # for target_file in Path(WORK / 'interclonedb/COVID19/target-2021/H').glob('*.tsv'):
    for target_file in Path(WORK / 'dedup/').rglob('*.tsv'):
        print('creating', target_file)
        target = pd.read_table(target_file, usecols=['sequence_id', 'sequence_aa', 'acdr3'])
        # extend CDR3 sequence to junction by adding conserved anchor residues before and after
        full_seqs_cdr3_pairs = zip(target['sequence_aa'].values.astype(str), target['acdr3'].values.astype(str))
        junction_aa = (full_seq[full_seq.find(cdr3)-1:full_seq.find(cdr3)+len(cdr3)+1] for full_seq, cdr3 in full_seqs_cdr3_pairs)
        target['junction_aa'] = np.fromiter(junction_aa, 'S99', len(target)).astype(str)
        target['repertoire_id'] = 'Healthy'
        target = target.loc[target['junction_aa'] != '']

        target_genes = pd.read_csv(GENE_DIR / f'healthy_{target_file.stem}_human_H.csv', usecols=['Id', 'v_gene', 'j_gene'])
        target_genes[['sequence_id', 'foo']] = target_genes['Id'].str.split(' ', expand=True)
        target_merged = target.merge(target_genes, left_on='sequence_id', right_on='sequence_id') \
            .rename(columns={'v_gene': 'v_call', 'j_gene': 'j_call'})

        target_parts.append(target_merged[['repertoire_id', 'sequence_id', 'junction_aa', 'v_call', 'j_call']])

    pd.concat(target_parts).to_csv(TARGET_INPUTS, index=False, sep='\t')#, columns=['repertoire_id', 'sequence_id', 'junction_aa', 'v_call', 'j_call'])

differences = range(11)
compairr_env = os.environ.copy()
compairr_env['LD_LIBRARY_PATH'] = compairr_env['CONDA_PREFIX'] + '/lib/'
interclone_hits = pd.read_table(INTERCLONE_RESULT, usecols=['Query', 'Templ'])
interclone_hits['Templ'] = interclone_hits['Templ'].str.slice(8)
print(interclone_hits.head())
print(len(interclone_hits), 'InterClone hits')
interclone_pairs = set(interclone_hits.apply(tuple, axis=1))

for max_diff in differences:
    log_file = OUTDIR / f'{max_diff}.log'
    out_file = OUTDIR / f'{max_diff}.out'
    pairs_file = OUTDIR / f'{max_diff}.pairs'
    compairr_cmd = [
        'compairr-1.7.0-linux-x86_64', '--existence',
        ENHAB_INPUTS, TARGET_INPUTS,
        '--ignore-counts',
        '--differences', str(max_diff),
        '--log', log_file,
        '--pairs', pairs_file,
        '--output', out_file,
    ]
    if not pairs_file.is_file():
        subprocess.run(compairr_cmd, env=compairr_env, check=True, text=True)
    hits = pd.read_table(pairs_file, usecols=['sequence_id_1', 'sequence_id_2'])
    if hits.empty:
        print(f'{len(hits)} hits for d={max_diff}')
        continue

    # hits['sequence_id_2'] = hits['sequence_id_2'].str.slice(8)
    if max_diff == 4:
        print(hits.head())
    pairs = set(hits.apply(tuple, axis=1))

    both = len(pairs.intersection(interclone_pairs))
    only_cp = len(pairs.difference(interclone_pairs))
    only_ic = len(interclone_pairs.difference(pairs))
    print(f'{len(hits)} hits for d={max_diff}\tboth: {both} \tonly InterClone: {only_ic}\tonly CompAIRR: {only_cp}')
