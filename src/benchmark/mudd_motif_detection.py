#!/usr/bin/env python

# Evaluate how well InterClone/CompAIRR can find the Mudd motif by checking the top public clusters
# invocation for IC (using defaults): ./src/benchmark/mudd_motif_detection.py
# invocation for CP: ./src/benchmark/mudd_motif_detection.py --rep-col "#cluster_no" --donor-col repertoire_id --seq-cols junction_aa v_call j_call --inputs work04/benchmark/cluster/compairr/cluster1.tsv --use-gene-names

# Has been adapted to evaluate detection of COVID-dominant clusters
# invocation for IC: ./src/benchmark/mudd_motif_detection.py --donor-col dataset_subgroup --seq-cols sequence_aa acdr1 acdr2 acdr3 dataset_name v_call j_call -c 100 -i work04/cocluster-HealthyCOVID/meta/A/meta.tsv
# invocation for CP: ./src/benchmark/mudd_motif_detection.py -c 20 --rep-col "#cluster_no" --donor-col repertoire_id --seq-cols junction_aa v_call j_call -i work04/benchmark/cluster/compairr/HeCo_cluster2.tsv --use-gene-names --dataset-col repertoire_id

import argparse
import csv
from pathlib import Path
import numpy as np
import pandas as pd
import scipy.stats

MUDD_MOTIF_CDR1 = r'SIFNT'
MUDD_MOTIF_CDR2 = r'LYKAGEL'
MUDD_MOTIF_CDR3 = r'CA[GAV].NYGGSQGNLIF'
WORK = Path('/mounts/scratch04/jwila/cdr_cluster/work/benchmark/cluster/compairr')
TCR_COMBINED = WORK / 'inputs_combined_prepped.tsv'

parser = argparse.ArgumentParser('Calculate and compare TCR clustering between InterClone and CompAIRR')
parser.add_argument('-i', '--inputs', type=Path, default='/mounts/scratch04/jwila/cdr_cluster/work/paper/cluster/postvac/meta-exp-ranked/A/meta.tsv',
                    help='result meta data')
parser.add_argument('-c', '--clusters', type=int, default=15, help='number of largest clusters to use')
parser.add_argument('-p', '--public-min', type=float, default=0.75,
                    help='minimum donor fraction in a cluster to be considered as public')
parser.add_argument('-o', '--out', type=Path, help='output TSV file path')
parser.add_argument('--rep-col', default='Representative')
parser.add_argument('--donor-col', default='donor')
parser.add_argument('--dataset-col', default='dataset_name')
parser.add_argument('--seq-cols', nargs='+', default=['sequence_aa'])
parser.add_argument('--use-gene-names', action='store_true',
                    help='use gene names in comparison instead of CDR sequences')
args = parser.parse_args()

# df = pd.read_table(TCR_COMBINED, usecols=['sequence_aa', 'donor'])
# motif_mask = df['sequence_aa'].str.contains(MUDD_MOTIF_CDR1) \
#             & df['sequence_aa'].str.contains(MUDD_MOTIF_CDR2) \
#             & df['sequence_aa'].str.contains(MUDD_MOTIF_CDR3)
# motif_total = motif_mask.sum()
# donors_total = df['donor'].nunique()
# print(f'Found {motif_total} Mudd motifs and {donors_total} donors')

meta = pd.read_table(args.inputs, usecols=[args.rep_col, 'sequence_id', args.donor_col] + args.seq_cols)
counts_all = meta['sequence_id'].str.split('-', 1, expand=True)[0].value_counts()
covid_prob = counts_all['COVID19'] / len(meta)
# print(counts_all)

if args.use_gene_names:
    motif_mask = meta['junction_aa'].str.contains(MUDD_MOTIF_CDR3) \
                & (meta['v_call'] == 'TRAV35') \
                & (meta['j_call'] == 'TRAJ42')
else:
    motif_mask = meta['sequence_aa'].str.contains(MUDD_MOTIF_CDR1) \
                & meta['sequence_aa'].str.contains(MUDD_MOTIF_CDR2) \
                & meta['sequence_aa'].str.contains(MUDD_MOTIF_CDR3)
motif_total = motif_mask.sum()
donors_total = meta[args.donor_col].nunique()
print(f'Found {motif_total} Mudd motifs and {donors_total} donors, {covid_prob*100:.2f}% COVID19')

def cal_z(covid_group, healthy_group, covid_total=counts_all['COVID19'], healthy_total=counts_all['Healthy']):
    if covid_group == 0 and healthy_group == 0:
        return None

    p_covid = covid_group / covid_total
    p_healthy = healthy_group / healthy_total
    return (p_covid - p_healthy) / np.sqrt((p_covid * (1.0 - p_covid) / covid_total) \
            + (p_healthy * (1.0 - p_healthy) / healthy_total))

clusters = meta[args.rep_col].value_counts()
true_hits = set()
pred_true = set()
pred_false = set()
print('Cumulative stats for top clusters:')
# header = ('top N', 'seqs', 'hits', 'pred T', 'pred F', 'public?', 'TP', 'FN', 'FP', 'prec', 'recall', 'f1')
header = ('top N', 'seqs', 'hits', 'pred T', 'pred F', 'covid%', 'zscore', '1-p', 'signif', 'TP', 'FN', 'FP', 'prec', 'recall', 'f1')
print('\t'.join(header))
results = [header]
# pseudo_cols = ['acdr1', 'acdr2', 'acdr3']
# pseudo_cols = ['junction_aa']
for idx, (rep, size) in enumerate(clusters.iloc[:args.clusters].iteritems()):
    cluster = meta.loc[meta[args.rep_col] == rep]
    donor_count = cluster[args.donor_col].nunique()
    donor_ratio = donor_count / donors_total
    is_public = donor_ratio >= args.public_min
    counts_group = cluster['sequence_id'].str.split('-', 1, expand=True)[0].value_counts()
    covid_count = counts_group['COVID19'] if 'COVID19' in counts_group else 0
    healthy_count = counts_group['Healthy'] if 'Healthy' in counts_group else 0
    z_score = cal_z(covid_count, healthy_count)
    cdf = scipy.stats.binom.cdf(k=covid_count, n=len(cluster), p=covid_prob)
    p_value = 1 - cdf
    if p_value < 0.001:
        sig = '***'
    elif p_value < 0.01:
        sig = '**'
    elif p_value < 0.05:
        sig = '*'
    else:
        sig = ''
    covid_ratio = cluster['sequence_id'].str.split('-', 1, expand=True)[0].value_counts(normalize=True)['COVID19']
    # print('COVID19', covid_ratio)
    # predictions
    # if is_public:
    # pseudo = cluster[pseudo_cols].apply('/'.join, axis=1).value_counts()
    # print(pseudo)
    genes = cluster[['v_call', 'j_call']].apply(' '.join, axis=1).value_counts()
    # print(genes)
    is_public = covid_ratio > 0.8
    junctions = ''#cluster['junction_aa'].value_counts().to_dict()
    if covid_ratio > 0.8:
        # junctions = ' '.join(junctions.index.tolist()) + '/' + ' '.join(cluster['acdr1'].value_counts().index) + '/' + ' '.join(cluster['acdr2'].value_counts().index)
        pred_true.update(cluster['sequence_id'])
    else:
        junctions = ''
        pred_false.update(cluster['sequence_id'])

    # true values
    if args.use_gene_names:
        # for CompAIRR
        motif_mask = cluster['junction_aa'].str.contains(MUDD_MOTIF_CDR3) \
                & (cluster['v_call'] == 'TRAV35') \
                & (cluster['j_call'] == 'TRAJ42')
    else:
        # for InterClone, using full length sequence
        motif_mask = cluster['sequence_aa'].str.contains(MUDD_MOTIF_CDR1) \
                   & cluster['sequence_aa'].str.contains(MUDD_MOTIF_CDR2) \
                   & cluster['sequence_aa'].str.contains(MUDD_MOTIF_CDR3)
    true_hits.update(cluster.loc[motif_mask, 'sequence_id'])

    TP = len(set(true_hits).intersection(pred_true))
    FN = len(set(true_hits).difference(pred_true))
    FP = len(set(pred_true).difference(true_hits))
    precision = TP / (TP + FP) if TP or FP else 0
    recall = TP / motif_total
    f1 = 2 * precision * recall / (precision + recall) if precision or recall else 0
    cumul_count = len(pred_true) + len(pred_false)
    # print(f'{idx+1}\t{cumul_count}\t{len(true_hits)}\t{len(pred_true)}\t{len(pred_false)}\t{"COVID" if is_public else ""}\t{TP}\t{FN}\t{FP}\t{precision:.3f}\t{recall:.3f}\t{f1:.3f}')
    print(f'{idx+1}\t{len(cluster)}\t{len(true_hits)}\t{len(pred_true)}\t{len(pred_false)}\t{covid_ratio:.3f}\t{z_score:>6.3f}\t{cdf:.3f}\t{sig}\t{TP}\t{FN}\t{FP}\t{precision:.3f}\t{recall:.3f}\t{f1:.3f}\t{junctions}')
    results.append([idx+1, cumul_count, len(true_hits), len(pred_true), len(pred_false), "PUBLIC" if is_public else "", TP, FN, FP, precision, recall, f1])

if args.out:
    with open(args.out, 'wt', newline='') as out_handle:
        result_writer = csv.writer(out_handle, delimiter='\t', lineterminator='\n')
        result_writer.writerows(results)
