#!/usr/bin/env python

# Creates stacked barplots to compare healthy and COVID19 search hits & clone counts

import argparse
from pathlib import Path
import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument('--covid-data', type=Path, required=True,
                    help='COVID19 search hits overview')
parser.add_argument('--healthy-data', type=Path, required=True,
                    help='healthy search hits overview')
parser.add_argument('--output', type=Path, required=True,
                    help='output file path')

args = parser.parse_args()

covid_df = pd.read_table(args.covid_data, usecols=['sequence_id', 'hits', 'clones'])
covid_hits = covid_df['hits'].sum()
covid_clones = covid_df['clones'].sum()
covid_df.rename(columns={
    'hits': f'COVID-19 hits\n({covid_hits})',
    'clones': f'COVID-19 clones\n({covid_clones})',
}, inplace=True)

healthy_df = pd.read_table(args.healthy_data, usecols=['sequence_id', 'hits', 'clones'])
healthy_hits = healthy_df['hits'].sum()
healthy_clones = healthy_df['clones'].sum()
healthy_df.rename(columns={
    'hits': f'healthy hits\n({healthy_hits})',
    'clones': f'healthy clones\n({healthy_clones})',
}, inplace=True)

merged = healthy_df.set_index('sequence_id').join(covid_df.set_index('sequence_id'), how='right', sort=False)
merged.index.rename('antibody', inplace=True)

ax = merged.T.plot(kind='bar', stacked=True, title='Enhancing antibody search', figsize=(8, 8), colormap='Set2', rot=0)
fig = ax.get_figure()
fig.savefig(args.output)
