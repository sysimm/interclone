#!/usr/bin/env python

import argparse
import csv
import os
import re
import subprocess
from itertools import chain
from pathlib import Path
import pandas as pd

parser = argparse.ArgumentParser('Calculate and compare TCR clustering between InterClone and CompAIRR')
parser.add_argument('--interclone', '--ic', type=Path, default='/mounts/scratch04/jwila/cdr_cluster/work/paper/cluster/postvac/meta-exp-ranked/A/meta.tsv', help='Interclone result meta.tsv')
parser.add_argument('-c', '--clusters', type=int, default=15, help='number of largest clusters to use')
parser.add_argument('-o', '--out', type=Path, help='output TSV file path')
parser.add_argument('-p', '--print', action='store_true', help='print out stats')
args = parser.parse_args()

WORK = Path('/mounts/scratch04/jwila/cdr_cluster/work/benchmark/cluster/compairr')
TCR_INPUTS = Path('/mounts/u31/jwila/cdr_cluster/inputs/tcr_vac_10x_nounk')
# TCR_PREPPED = Path('/mounts/scratch04/jwila/cdr_cluster/work/paper/cluster/postvac/prep')
BASE = Path('/mounts/scratch04/jwila/cdr_cluster/work/')
TCR_COMBINED = WORK / 'HeCo_combined_prepped.tsv'
FASTA_COMBINED = WORK / 'HeCo_combined_prepped.fa'
# FILE_INDEX = WORK / 'files.idx'
# name = 'postvac'
input_folders = ('25_28', '27', '4_17', '8_13_15')
# input_folders = ('prevac',)
input_folders = ('Healthy-45donors', 'COVID19-98donors')
# INTERCLONE_RESULT = Path('/mounts/scratch04/jwila/cdr_cluster/work/public_db/tcrvac10x/cluster')
# INTERCLONE_RESULT = Path('/mounts/scratch04/jwila/cdr_cluster/work/paper/cluster/postvac/meta-exp-ranked/A/meta.tsv')
INTERCLONE_RESULT = args.interclone
TOP_CLUSTERS = args.clusters
MUDD_MOTIF_CDR1 = r'SIFNT'
MUDD_MOTIF_CDR2 = r'LYKAGEL'
MUDD_MOTIF_CDR3 = r'CA[GAV].NYGGSQGNLIF'

if not TCR_COMBINED.is_file() or not FASTA_COMBINED.is_file():
    in_files = chain.from_iterable((BASE / in_name).glob('*/A/*.tsv') for in_name in input_folders)
    all_inputs = []
    # file_index = {}
    for file_idx, input_file in enumerate(in_files):
        # file_index[file_idx] = input_file
        df = pd.read_table(input_file, usecols=['sequence_id', 'cdr3', 'v_call', 'j_call', 'sequence_aa', 'dataset_subgroup', 'clone_count']).rename(columns={'cdr3': 'junction_aa', 'clone_count': 'duplicate_count'})
        # df['repertoire_id'] = df['AssignedTag'].str.lstrip('_').str.split('_', 1, expand=True)[0]
        df['repertoire_id'] = df['dataset_subgroup']
        # df.rename(columns={'donor': 'repertoire_id'}, inplace=True)
        # df['sequence_id'] = f'{file_idx}_' + df['sequence_id']
        alpha = df['v_call'].str.contains('TRA')
        all_inputs.append(df[alpha])

    # pd.DataFrame.from_dict(file_index, orient='index').to_csv(FILE_INDEX, sep='\t', header=False)

    combined = pd.concat(all_inputs)
    motif_mask = combined['sequence_aa'].str.contains(MUDD_MOTIF_CDR1) \
               & combined['sequence_aa'].str.contains(MUDD_MOTIF_CDR2) \
               & combined['sequence_aa'].str.contains(MUDD_MOTIF_CDR3)
    motif_total = motif_mask.sum()
    if not TCR_COMBINED.is_file():
        combined.to_csv(TCR_COMBINED, index=False, sep='\t')
    if not FASTA_COMBINED.is_file():
        fasta = '>' + combined['repertoire_id'].astype(str) + '_' + combined['sequence_id'].astype(str) \
            + ' ' + combined['junction_aa'].astype(str) + '_' + combined['v_call'].astype(str) \
            + '_' + combined['j_call'].astype(str) + '\n' + combined['sequence_aa']
        with open(FASTA_COMBINED, "w") as outfh:
            outfh.write('\n'.join(fasta))
            outfh.write('\n')
else:
    df = pd.read_table(TCR_COMBINED, usecols=['sequence_aa'])
    motif_mask = df['sequence_aa'].str.contains(MUDD_MOTIF_CDR1) \
               & df['sequence_aa'].str.contains(MUDD_MOTIF_CDR2) \
               & df['sequence_aa'].str.contains(MUDD_MOTIF_CDR3)
    motif_total = motif_mask.sum()
    print(f'Found {motif_total} Mudd motifs')

compairr_hits = {}
for d in range(-1, 6):
    log_file = WORK / f'HeCo_cluster{d}.log'
    out_file = WORK / f'HeCo_cluster{d}.tsv'
    summary_file = WORK / f'HeCo_summary{d}.tsv'
    # if summary_file.is_file():
    #     continue
    compairr_cmd = [
        'compairr-1.7.0-linux-x86_64', '--cluster',
        TCR_COMBINED,
        # '--ignore-counts',
        '--log', log_file,
        '--output', out_file,
        '-d', str(abs(d)),
    ]
    if d == -1:
        compairr_cmd.append('-i')
    compairr_env = os.environ.copy()
    compairr_env['LD_LIBRARY_PATH'] = compairr_env['CONDA_PREFIX'] + '/lib/'
    if not out_file.is_file():
        subprocess.run(compairr_cmd, env=compairr_env, check=True, text=True)

    hits = pd.read_table(out_file, dtype={'#cluster_no': str})
    # print(hits.head())

    # hits = hits.loc[hits.repertoire_id != 'UNK']
    donors_all = hits['repertoire_id'].nunique()
    summary = []
    # print('CompAIRR clusters:')
    for idx, (rep, cluster) in enumerate(hits.groupby('#cluster_no')):
        # if idx <= 10:
            # break
            # print(f'{rep}\t{len(cluster)}\t', cluster['junction_aa'].unique(), cluster['repertoire_id'].value_counts().to_dict())

        # TODO: retrieve sequence_aa_count and CDRs from original inputs
        values = [rep, len(cluster), int(cluster['duplicate_count'].sum()), cluster['repertoire_id'].nunique(), donors_all] \
                + cluster.iloc[0][['v_call', 'j_call', 'junction_aa', 'v_call', 'j_call']].tolist()
                # + cluster.iloc[0][['acdr1', 'acdr2', 'acdr3']].tolist()
        summary.append(values)

    # label V and J gene as CDR1/2 because this data doesn't have had ANARCI run on it
    columns = ['representative', 'members', 'cluster_size', 'donors_group', 'donors_all', 'cdr1', 'cdr2', 'cdr3', 'v_call', 'j_call']
    cp_result = pd.DataFrame(summary, columns=columns).sort_values('cluster_size', ascending=False)
    cp_hits = set()
    top_reps = cp_result.iloc[:TOP_CLUSTERS]['representative']
    motif_total_cp = nonmotif_total_cp = 0
    for _, rep in top_reps.iteritems():
        cluster = hits.loc[hits['#cluster_no'] == rep]
        junctions = cluster['junction_aa'].value_counts()
        motif_count_cp = sum(count for junction, count in junctions.iteritems() if re.match(MUDD_MOTIF_CDR3, junction))
        nonmotif_count = sum(junctions) - motif_count_cp
        if motif_count_cp < nonmotif_count: # alternatively, check clone count
            # motif is not the majority, skip
            continue
        cp_hits.update(cluster['sequence_id'])
        motif_total_cp += motif_count_cp
        nonmotif_total_cp += nonmotif_count
    compairr_hits[d] = cp_hits, motif_total_cp, nonmotif_total_cp
    if not summary_file.is_file():
        cp_result.to_csv(summary_file, sep='\t', index=False)

# compute overlap
ic_result = pd.read_table(INTERCLONE_RESULT, usecols=['Representative', 'sequence_id', 'donor',
            'author_sequence_id', 'junction_aa', 'sequence_aa_count', 'acdr1', 'acdr2', 'acdr3'])
# ic_result = pd.read_table(INTERCLONE_RESULT).sort_values('cluster_size', ascending=False)
# ic_result['compairr_id'] = ic_result.donor.astype(str) + '_' + ic_result.author_sequence_id
# ic_result['donor'] = ic_result['AssignedTag'].str.lstrip('_').str.split('_', 1, expand=True)[0]
# filter out prevac
# ic_result = ic_result.loc[ic_result.donor != 'UNK']

clusters = ic_result['Representative'].value_counts()
interclone_hits = set()
ic_motif_total = ic_nonmotif_total = 0
for rep, size in clusters.iloc[:TOP_CLUSTERS].iteritems():
    cluster = ic_result.loc[ic_result['Representative'] == rep]
    junctions = cluster['junction_aa'].value_counts()
    motif_count_ic = sum([count for junction, count in junctions.iteritems() if re.match(MUDD_MOTIF_CDR3, junction)])
    nonmotif_count = sum(junctions) - motif_count_ic
    if motif_count_ic < nonmotif_count: # alternatively, check clone count
        # motif is not the majority, skip
        continue
    interclone_hits.update(cluster['sequence_id'])
    ic_motif_total += motif_count_ic
    ic_nonmotif_total += nonmotif_count
    # print(f'{rep}\t{size}\t', cluster['junction_aa'].value_counts().to_dict(), cluster['donor'].value_counts().to_dict())
if args.print:
    print('top', TOP_CLUSTERS, 'clusters')
precision_ic = ic_motif_total / (ic_motif_total + ic_nonmotif_total)
recall_ic = ic_motif_total / motif_total
f1_ic = 2 * precision_ic * recall_ic / (precision_ic + recall_ic)
if args.print:
    print(f'IC hits: {len(interclone_hits)}, {ic_motif_total} motifs, {ic_nonmotif_total} others = {precision_ic:.3f} precision, {recall_ic:.3f} recall, {f1_ic:.3f} F1')

results = []
for d, (cp_hits, motif_count_cp, nonmotif_count_cp) in compairr_hits.items():
    both = len(cp_hits.intersection(interclone_hits))
    only_cp = len(cp_hits.difference(interclone_hits))
    only_ic = len(interclone_hits.difference(cp_hits))
    precision_cp = motif_count_cp / (motif_count_cp + nonmotif_count_cp)
    recall_cp = motif_count_cp / motif_total
    f1_cp = 2 * precision_cp * recall_cp / (precision_cp + recall_cp)
    results.append([d, len(cp_hits), both, only_ic, only_cp, precision_cp, recall_cp, f1_cp])
    if args.print:
        print(f'{len(cp_hits)} hits for d={d}\tboth: {both} \tonly InterClone: {only_ic}\tonly CompAIRR: {only_cp}\t{motif_count_cp} motifs\t{nonmotif_count_cp} others = {precision_cp:.3f} precision, {recall_cp:.3f} recall, {f1_cp:.3f} F1')

header = ['d', 'CP hits', 'both', 'IC only', 'CP only', 'precision', 'recall', 'f1']
if args.out:
    with open(args.out, 'wt') as out_handle:
        csv_writer = csv.writer(out_handle, delimiter='\t', lineterminator='\n')
        csv_writer.writerow(header)
        for result in results:
            csv_writer.writerow(str(item) for item in result)

if args.print:
    print('\t'.join(header))
    for result in results:
        print('\t'.join(str(item) for item in result))
