#!/usr/bin/env python

import json
import os
import re
import subprocess
from itertools import chain
from pathlib import Path
import pandas as pd
import numpy as np
import scipy.stats

WORK = Path('/mounts/scratch04/jwila/cdr_cluster/work/benchmark/cluster/cdhit')
COMPAIRR = WORK.parent / 'compairr'
# INPUTS = Path('/mounts/u31/jwila/cdr_cluster/inputs/tcr_vac_10x_donor')
INPUTS = Path('/mounts/scratch04/jwila/cdr_cluster/work/tcr_vac_10x')
# TCR_COMBINED = WORK / 'inputs_combined.tsv'
META = COMPAIRR / 'HeCo_combined_prepped.tsv'
# FASTA_COMBINED = COMPAIRR / 'inputs_combined.fa'
FASTA_COMBINED = COMPAIRR / 'HeCo_combined_prepped.fa'
datasets = ('25_28', '27', '4_17', '8_13_15', 'prevac')
TSV_FILES = chain.from_iterable((INPUTS / ds / 'A').glob('*.tsv') for ds in datasets)
# INTERCLONE_RESULT = Path('/mounts/scratch04/jwila/cdr_cluster/work/public_db/tcrvac10x/cluster')
sid_cutoffs = (80, 90, 95)
N_CLUSTERS = 15
MUDD_MOTIF_CDR3 = r'CA[GAV].NYGGSQGNLIF'

# meta = pd.concat([pd.read_table(in_file, usecols=['sequence_id', 'author_sequence_id', 'donor', 'acdr1', 'acdr2', 'acdr3', 'v_call', 'j_call']) for in_file in TSV_FILES])
meta = pd.read_table(META)#, usecols=['sequence_id', 'author_sequence_id', 'donor', 'acdr1', 'acdr2', 'acdr3', 'v_call', 'j_call'])
meta.index = meta['repertoire_id'] + '_' + meta['sequence_id']
counts_all = meta['sequence_id'].str.split('-', 1, expand=True)[0].value_counts()
# print(counts_all)
covid_prob = counts_all['COVID19'] / len(meta)
# print(meta.head())

def cal_z(covid_group, healthy_group, covid_total=counts_all['COVID19'], healthy_total=counts_all['Healthy']):
    if covid_group == 0 and healthy_group == 0:
        return None

    p_covid = covid_group / covid_total
    p_healthy = healthy_group / healthy_total
    return (p_covid - p_healthy) / np.sqrt((p_covid * (1.0 - p_covid) / covid_total) \
            + (p_healthy * (1.0 - p_healthy) / healthy_total))

for sid in sid_cutoffs:
    print('SID', sid)
    # print('SID', sid)
    out_file = WORK / f'HeCo_combined_A.{sid}.fa'
    clstr_file = out_file.with_suffix('.fa.clstr')
    cdhit_cmd = [
        'cd-hit',
        '-i', FASTA_COMBINED,
        '-o', out_file,
        '-c', f'0.{sid}',
        '-d', '0',
        '-T', '8',
    ]
    if not out_file.is_file():
        subprocess.run(cdhit_cmd, check=True)

    sorted_clstr_file = WORK / f'HeCo_combined_A.{sid}.sorted.fa.clstr'
    if not sorted_clstr_file.is_file():
        with open(clstr_file) as clusters, \
             open(sorted_clstr_file, 'wt') as sorted_clusters:
            subprocess.run('clstr_sort_by.pl', check=True, stdin=clusters, stdout=sorted_clusters)

    cluster_id = None
    clusters = {}
    with open(sorted_clstr_file) as sorted_clusters:
        for line in sorted_clusters:
            if line.startswith('>Cluster'):
                cluster_id = int(line.strip().split()[-1])
                continue
            # if cluster_id >= N_CLUSTERS:
            #     break
            parts = line.split()
            seq_id = parts[2][1:-3]
            is_rep = parts[-1] == '*'
            clusters[seq_id] = int(cluster_id)
            # clusters.append((cluster_id, seq_id, is_rep))
            # if is_rep:
            #     print(cluster_id, meta.loc[seq_id])

    merged = pd.DataFrame.from_dict(clusters, orient='index', columns=['cluster']).join(meta)
    donors_total = merged['repertoire_id'].nunique()
    motif_mask = merged['junction_aa'].str.contains(MUDD_MOTIF_CDR3) \
               & (merged['v_call'] == 'TRAV35') \
               & (merged['j_call'] == 'TRAJ42')
    motif_total = motif_mask.sum()
    # print(merged)
    # print('cluster\tsize\tdonors\tmotif\tjunction\tcount\tV gene\tcount\tJ gene\tcount\tsize')
    results = []
    true_hits = set()
    pred_true = set()
    pred_false = set()
    header = ('top N', 'seqs', 'covid%', 'zscore', '1-p', 'signif')
    print('\t'.join(header))
    for cluster_id, cluster in merged.groupby('cluster'):
        if cluster_id >= 20:
            break
        # print(cluster_id, len(cluster))
        # junctions = cluster['junction_aa'].value_counts()
        # v_genes = cluster['v_call'].value_counts()
        # j_genes = cluster['j_call'].value_counts()
        # top_junction = cluster['junction_aa'].value_counts().index[0]
        # top_vgene = cluster['v_call'].value_counts().index[0]
        # top_jgene = cluster['j_call'].value_counts().index[0]
        donor_count = cluster['repertoire_id'].nunique()
        donor_ratio = donor_count / donors_total
        counts_group = cluster['sequence_id'].str.split('-', 1, expand=True)[0].value_counts()
        covid_count = counts_group['COVID19'] if 'COVID19' in counts_group else 0
        healthy_count = counts_group['Healthy'] if 'Healthy' in counts_group else 0
        z_score = cal_z(covid_count, healthy_count)
        cdf = scipy.stats.binom.cdf(k=covid_count, n=len(cluster), p=covid_prob)
        p_value = 1 - cdf
        if p_value < 0.001:
            sig = '***'
        elif p_value < 0.01:
            sig = '**'
        elif p_value < 0.05:
            sig = '*'
        else:
            sig = ''

        if z_score > 5:
            junctions = cluster['junction_aa'].value_counts().to_dict()
        else:
            junctions = ''
        covid_ratio = cluster['sequence_id'].str.split('-', 1, expand=True)[0].value_counts(normalize=True)['COVID19']
        print(f'{cluster_id+1}\t{len(cluster)}\t{covid_ratio:.3f}\t{z_score:>6.3f}\t{cdf:.3f}\t{sig}\t{junctions}')
        continue

        # Mudd motif detection in public clusters
        is_public = donor_ratio >= .75 # args.public_min
        # predictions
        if is_public:
            pred_true.update(group['sequence_id'])
            print(group['junction_aa'].value_counts())
        else:
            pred_false.update(group['sequence_id'])

        # true values
        motif_mask = group['junction_aa'].str.contains(MUDD_MOTIF_CDR3) \
                & (group['v_call'] == 'TRAV35') \
                & (group['j_call'] == 'TRAJ42')
        true_hits.update(group.loc[motif_mask, 'sequence_id'])

        TP = len(set(true_hits).intersection(pred_true))
        FN = len(set(true_hits).difference(pred_true))
        FP = len(set(pred_true).difference(true_hits))
        precision = TP / (TP + FP) if TP or FP else 0
        recall = TP / motif_total
        f1 = 2 * precision * recall / (precision + recall) if precision or recall else 0
        cumul_count = len(pred_true) + len(pred_false)

        # junction = junctions.index[0]
        # print(group.loc[group['duplicate_count'].isna()])
        size = group['duplicate_count'].sum()
        motif = '*' if re.match(r'CA[GAV].NYGGSQGNLIF', junction) else ' '
        result = [cluster_id, len(group), group['repertoire_id'].nunique(), junction, v_genes.index[0], j_genes.index[0], int(size)]
        # result = [cluster_id, len(group), group['repertoire_id'].nunique(), motif, junction, len(junctions), v_genes.index[0], len(v_genes), j_genes.index[0], len(j_genes), int(size)]
        results.append(result)
        # print('\t'.join([str(r) for r in result]))
        # print(f'Cluster {cluster_id}, size {len(group)}: {junctions.index[0]} ({len(junctions)}), {v_genes.index[0]} ({len(v_genes)}), {j_genes.index[0]} ({len(j_genes)})')
    cluster_stats = WORK / f'clusters_{sid}.tsv'
    stats = pd.DataFrame(results, columns=['representative', 'nmem', 'ngroup', 'cdr1', 'cdr2', 'cdr3', 'Cluster Size'])
    stats['maxgroup'] = 9
    stats['v_call'] = ''
    stats['j_call'] = ''
    stats.sort_values('Cluster Size', ascending=False).to_csv(cluster_stats, sep='\t', index=False)
    # break
