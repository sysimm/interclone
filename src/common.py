import contextlib
import json
import os
import sys
from collections import defaultdict
from collections.abc import Iterable
from pathlib import Path


BASE_DIR = Path(__file__).resolve().parent.parent
SYSIMM_RUNNER_LSF = BASE_DIR / 'sysimm_runner' / 'bin' / 'sysimm_runner_lsf'
SYSIMM_RUNNER_SLURM = BASE_DIR / 'sysimm_runner' / 'bin' / 'sysimm_runner_slurm'
RUNNERS = {
    'lsf': SYSIMM_RUNNER_LSF,
    'slurm': SYSIMM_RUNNER_SLURM,
}

# SOURCE: https://stackoverflow.com/a/46407326/15744300
#
# Posix based file locking (Linux, Ubuntu, MacOS, etc.)
#   Only allows locking on writable files, might cause
#   strange results for reading.
import fcntl

def lock_file(file_handle):
    if file_handle.writable():
        fcntl.lockf(file_handle, fcntl.LOCK_EX)

def unlock_file(file_handle):
    if file_handle.writable():
        fcntl.lockf(file_handle, fcntl.LOCK_UN)

# Class for ensuring that all file operations are atomic, treat
# initialization like a standard call to 'open' that happens to be atomic.
# This file opener *must* be used in a "with" block.
# WARNING: Advisory locking only. All competing processes have to use this
class AtomicOpen:
    def __init__(self, path, *args, **kwargs):
        # Open the file and acquire a lock on the file before operating
        self.file = open(path,*args, **kwargs)
        # Lock the opened file
        lock_file(self.file)

    # Return the opened file object (knowing a lock has been obtained).
    def __enter__(self, *args, **kwargs):
        return self.file

    # Unlock the file and close the file object.
    def __exit__(self, exc_type=None, exc_value=None, traceback=None):
        # Flush to make sure all buffered contents are written to file.
        self.file.flush()
        os.fsync(self.file.fileno())
        # Release the lock on the file.
        unlock_file(self.file)
        self.file.close()
        # Handle exceptions that may have come up during execution, by
        # default any exceptions are raised to the user.
        if exc_type is not None:
            return False
        else:
            return True


ALLOWED_CHAINS = ('H', 'L', 'A', 'B', 'HL', 'AB')

def find_files_by_chain(base_paths, chains, pattern, direct_child=False):
    """ Returns a list of files/folders matching a given pattern from the given paths.
        Checks the path itself as well as its children. Requires the
        chain folder to be a parent of the files/folders.
    """
    out_paths = defaultdict(list)

    # make it work for single paths
    if not isinstance(base_paths, Iterable):
        base_paths = [base_paths]

    for base_path in base_paths:
        if not base_path.is_dir():
            continue
        db_chain = base_path.name
        if db_chain in ALLOWED_CHAINS and (not chains or db_chain in chains):
            if direct_child:
                # only allow results directly below a chain folder
                out_paths[db_chain].extend(base_path.glob(pattern))
            else:
                # go as deep as you like
                out_paths[db_chain].extend(base_path.rglob(pattern))
        else:
            # try subfolders
            sub_results = find_files_by_chain(base_path.glob('?'), chains, pattern, direct_child)
            for chain, db_paths in sub_results.items():
                out_paths[chain].extend(db_paths)

    return out_paths


@contextlib.contextmanager
def smart_open(filename=None):
    """ replacement for the open() function that can handle stdout """

    if filename and filename != '-':
        handle = open(filename, 'w', newline='')
    else:
        handle = sys.stdout

    try:
        yield handle
    finally:
        if handle is not sys.stdout:
            handle.close()


def write_progress(progress_data, progress_path):
    if not progress_path:
        return

    with open(progress_path, "w") as handle:
        json.dump(progress_data, handle)


def update_progress(progress_path, num_seq=0, status=None):
    if not progress_path:
        return # not set initially

    # open atomically because this may be called by concurrent processes
    with AtomicOpen(progress_path, 'r+') as status_handle:
        progress_data = json.load(status_handle)

        progress_data['numSeq'] = progress_data.get('numSeq', 0) + num_seq
        if status:
            progress_data['status'] = status
        else:
            progress_data['currentStep'] += 1

        status_handle.seek(0)
        status_handle.truncate()
        json.dump(progress_data, status_handle)
