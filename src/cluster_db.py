#!/usr/bin/env python3
# coding: utf-8
import argparse
import os
import shutil
import subprocess
import sys
import tempfile
import pandas as pd
from pathlib import Path

from common import find_files_by_chain, ALLOWED_CHAINS, write_progress, update_progress


def create_mmseqs_dbs(fasta_list, output_folder):
    """ Create mmseqs database and index for faster searching and clustering """

    output_folder.mkdir(parents=True, exist_ok=True)

    seq_db = output_folder / "seq.db"
    cmd = ["mmseqs", "createdb"] + fasta_list + [seq_db, "--dbtype", "1"]

    subprocess.run(cmd, check=True, capture_output=True, text=True)

    return seq_db.is_file()


def cluster_db(input_folder, output_folder, sid, cov, progress_path):

    tempdir = tempfile.mkdtemp()
    input_db = input_folder / "seq.db"
    output_db = output_folder / "cluster.db"

    cmd = [
        "mmseqs", "cluster",
        input_db, output_db, tempdir,
        "--min-seq-id", str(sid),
        "-c", str(cov)
    ]
    subprocess.run(cmd, check=True, capture_output=True, text=True)
    update_progress(progress_path)

    out_tsv_tmp = tempdir + "/mmseqs_clusters.tsv"

    cmd = [
        "mmseqs", "createtsv",
        input_db, input_db,
        output_db, out_tsv_tmp
    ]
    subprocess.run(cmd, check=True, capture_output=True, text=True)
    update_progress(progress_path)

    out_tsv = output_folder / "clusters.tsv"
    header = ['Representative', 'Member']
    raw = pd.read_table(out_tsv_tmp, header=None, names=('rep', 'mem'))
    # cut off CDR lengths
    raw['Representative'] = raw['rep'].str.rsplit('_', 3, expand=True)[0]
    raw['Member'] = raw['mem'].str.rsplit('_', 3, expand=True)[0]
    raw.sort_values(header).to_csv(out_tsv, index=False, columns=header, sep='\t')
    result_count = len(raw)

    shutil.rmtree(tempdir)
    update_progress(progress_path, result_count)


def main():
    parser = argparse.ArgumentParser(description='cluster multiple datasets')
    parser.add_argument('-i', dest='input_folders', nargs='+', type=Path, required=True,
                        help='a folder containing one or more DB folders')
    parser.add_argument('-o', dest='out_folder', type=Path, required=True,
                        help='output_folder')
    parser.add_argument('--sid', type=int, default=80,
                        help='cdr1 % sequence identity cutoff [0-100]')
    parser.add_argument('--cov', type=int, default=90,
                        help='min % align coverage [0-100]')
    parser.add_argument('-c', '--chain', choices=ALLOWED_CHAINS,
                        help='chain to use for clustering (default: determine automatically)')
    parser.add_argument('--clean', action="store_true",
                        help='remove output folder if it exists')
    parser.add_argument('--threads', default=8, type=int, help='number of parallel threads to run')

    args = parser.parse_args()

    dout = args.out_folder

    if args.clean and dout.is_dir():
        shutil.rmtree(dout)

    dout.mkdir(exist_ok=True, parents=True)

    #initialize progress with minimum data length
    progress_data = {
        'totalSteps': 2,
        'currentStep': 0,
        'status': 'pending', # for error reporting
    }
    progress_path = dout / "progress.json"
    write_progress(progress_data, progress_path)

    inputs = find_files_by_chain(args.input_folders, args.chain, '*-pseudo_joined.fa')

    if not inputs:
        print('Could not find any inputs for chains', args.chain)
        update_progress(progress_path, status='error')
        sys.exit(1)

    if args.chain and not set(inputs) >= set(args.chain):
        print(f'Could not find databases for all requested chains. Found {set(inputs)}, requested {args.chain}')
        update_progress(progress_path, status='error')
        sys.exit(1)

    if args.cov < 0 or args.cov > 100:
        print('--cov must be between 0 and 100')
        update_progress(progress_path, status='error')
        sys.exit(1)

    os.environ['MMSEQS_NUM_THREADS'] = str(args.threads)

    #initialize progress with actual data length
    progress_data['totalSteps'] = len(inputs) * 4
    progress_data['status'] = 'running'
    write_progress(progress_data, progress_path)

    fsid = args.sid / 100
    fcov = args.cov / 100

    for chain, fasta_list in inputs.items():
        chain_out = dout / chain
        if not chain_out.is_dir():
            chain_out.mkdir(parents=True, exist_ok=True)

        db_folder = chain_out / "mmseqs"
        cluster_folder = chain_out / "clusters"

        if not cluster_folder.is_dir():
            cluster_folder.mkdir(parents=True, exist_ok=True)

        mmseqs_success = create_mmseqs_dbs(fasta_list, db_folder)
        update_progress(progress_path)

        if mmseqs_success:
            cluster_db(db_folder, cluster_folder, fsid, fcov, progress_path)
        else:
            print("db prep failed for chain", chain)

    update_progress(progress_path, status='finished')


if __name__ == '__main__':
    main()
