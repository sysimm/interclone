#!/usr/bin/env python
# coding: utf-8
import argparse
from pathlib import Path

import pandas as pd
import scipy.io
import numpy as np
from scipy.stats import norm
import sys
import matplotlib.pyplot as plt
from statistics import median
from scipy.optimize import curve_fit

def extract_hashtags(feature_folder):
    mat_path = feature_folder / "matrix.mtx.gz"
    barcodes_path = feature_folder / "barcodes.tsv.gz"
    features_path = feature_folder / "features.tsv.gz"

    barcodes = pd.read_table(barcodes_path, header=None, usecols=[0], names=['cell_id'])
    features = pd.read_table(features_path, header=None, names=['id', 'gene_name', 'feature_type'])
    mask = features['feature_type'] != "Gene Expression"
    filtered = features.loc[mask]

    mat = scipy.io.mmread(mat_path).toarray()
    mat = mat[mask]

    hashtags = pd.DataFrame(mat.T, columns=filtered['gene_name'])
    combined = pd.concat([barcodes, hashtags], axis=1)
    return combined


def assign_hashtags(df, outfile, col_pattern='Hashtag', threshold=6.0):
    hashtag_cols = [col for col in df.columns if col_pattern in col]

    cutoff = pow(2, threshold)
    df['AssignedTag'] = "UNK"
    df['best_hashtag_value'] = df[hashtag_cols].max(axis=1)
    df['best_hashtag'] = df[hashtag_cols].idxmax(axis=1)

    # df.where preserves the value (UNK) if the condition is true
    df['AssignedTag'].where(df['best_hashtag_value'] < cutoff, df['best_hashtag'], axis=0, inplace=True)
    df.drop(columns=['best_hashtag', 'best_hashtag_value'], inplace=True)
    df.to_csv(outfile, sep='\t', index=False)
    print(f'write output file {outfile}')




def midpoint(p1, p2):
    return (p1+p2)/2

def gaussian(x, mean, amplitude, standard_deviation):
    return amplitude * np.exp( - (x - mean)**2 / (2*standard_deviation ** 2))





def assign_hashtags(df, outfile, threshold, col_pattern='Hashtag'):
    hashtag_cols = [col for col in df.columns if col_pattern in col]

    cutoff = pow(2, threshold)
    df['AssignedTag'] = "UNK"
    df['best_hashtag_value'] = df[hashtag_cols].max(axis=1)
    df['best_hashtag'] = df[hashtag_cols].idxmax(axis=1)

    # df.where preserves the value (UNK) if the condition is true
    df['AssignedTag'].where(df['best_hashtag_value'] < cutoff, df['best_hashtag'], axis=0, inplace=True)
    df.drop(columns=['best_hashtag', 'best_hashtag_value'], inplace=True)
    df.to_csv(outfile, sep='\t', index=False)
    print(f'write output file {outfile}')


def get_hashtag_threshold(df, outfile, col_pattern='Hashtag', show_plot=False):

    hashtag_cols = [col for col in df.columns if col_pattern in col]


    nbin=20
    bcut=10
    nobs=len(df)
    nhashtag = len(hashtag_cols)

    cutoffs=[]
    for h in hashtag_cols:

        #print(h)
        hashtag_expr = df[h].astype(float).values


        # take log2(expression values + 1)
        hashtag_expr_log = np.log2(hashtag_expr + 1.0)

        # define the range of log2(x+1)  vaues
        xmin=min(hashtag_expr_log)
        xmax=max(hashtag_expr_log)

        # define the midpoint
        meanval= midpoint(xmin, xmax)

        # collect values below the midpoint
        low_values = hashtag_expr_log [ hashtag_expr_log <= meanval]

        # collect values above the midpoint
        high_values = hashtag_expr_log [ hashtag_expr_log >= meanval]



        # compute the mean and sdev of low values
        mu_low, sigma_low = norm.fit(low_values)


        ### beginning of low_value plot
        #plt.hist(low_values, bins=10, density=True, alpha=0.6, color='g')
        #xmin, xmax = plt.xlim()
        #x = np.linspace(xmin, xmax, 100)
        #p = norm.pdf(x, mu_low, sigma_low)
        #plt.plot(x, p, 'k', linewidth=2)
        #title = "Fit results: mu = %.2f,  std = %.2f" % (mu_low, sigma_low)
        #plt.title(title)
        #plt.show()



        ### end of low_value plot

        # compute the mean and sdev of high values
        mu_high, sigma_high = norm.fit(high_values)
        
        #show histogram of log2-scalled data 
        log_hist, bins = np.histogram(hashtag_expr_log,bins=nbin)

        # get the centers of the bins
        bin_centers = bins[:-1] + np.diff(bins) / 2


        # get the peaks of the two histograms (low and high)
        ymax_low = log_hist[0:bcut+1].max()
        ymax_high = log_hist[bcut:nbin].max()


        gauss_low, _ = curve_fit(gaussian, bin_centers[0:bcut+1], log_hist[0:bcut+1], p0=[mu_low,ymax_low,sigma_low])
        gauss_high, _ = curve_fit(gaussian, bin_centers[bcut:nbin], log_hist[bcut:nbin], p0=[mu_high,ymax_high,sigma_high])



        # plot the histogram of raw data


        # define a set of points for the fitted curves
        xpoints = np.linspace(bins[0], bins[-1], 2*nbin)

        glow=gaussian(xpoints, *gauss_low)
        ghigh=gaussian(xpoints, *gauss_high)


        max_index_low = np.argmax(glow)

        max_index_high = np.argmax(ghigh)

        gsum = [x + y for x, y in zip(glow, ghigh)]

        #print("max_index_low", max_index_low)
        #print("max_index_high", max_index_high)
        #print("gsum", gsum)

        #print("gsum", gsum[max_index_low+1:max_index_high])
        isuggested = max_index_low + np.argmin(gsum[max_index_low+1:max_index_high])

        if show_plot:
            # plot the fitted curves
            plt.bar(bins[:-1],log_hist,label='log2(x+1)',linestyle = 'None',color='gray')
            plt.plot(xpoints, gaussian(xpoints, *gauss_low),label='low fit' )
            plt.plot(xpoints, gaussian(xpoints, *gauss_high),label='high fit' )
            plt.title(h)
            plt.axvline(x=xpoints[isuggested])
            plt.show()

        print("suggested",xpoints[isuggested])
        scutoff = input("Please enter a cutoff to override (retirn to accept): ")
        if scutoff:
            cutoff= float(scutoff)
        else:
            cutoff = xpoints[isuggested]

        print("Will use ", cutoff) 
        cutoffs.append(cutoff)

    return median(cutoffs)



def assign_multi_hashtags(df, outfile, threshold, col_pattern='Hashtag'):
    hashtag_cols = [col for col in df.columns if col_pattern in col]

    cutoff = pow(2, threshold)
    df['AssignedTag'] = "UNK"
    #df['best_hashtag_value'] = df[hashtag_cols].max(axis=1)
    #df['best_hashtag'] = df[hashtag_cols].idxmax(axis=1)

    for index, row in df.iterrows():
        v = []
        for h in hashtag_cols :
            if row[h] >= cutoff:
                v.append(h)
        if len(v) == 1:
            #print("Single",index,v[0])
            df.at[index, 'AssignedTag'] = v[0]
        #elif len(v) == 0:
        #    print("Zero",index)
        else :
            #print("Muliple",index," ".join(v))
            df.at[index, 'AssignedTag'] = "Multiplet:" + " ".join(v)
    #df['AssignedTag'].where(df['best_hashtag_value'] < cutoff, df['best_hashtag'], axis=0, inplace=True)
    #df.drop(columns=['best_hashtag', 'best_hashtag_value'], inplace=True)
    df.to_csv(outfile, sep='\t', index=False)
    print(f'write output file {outfile}')





if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='merge hashtags with 10X AIRR data')
    parser.add_argument('-i', dest='input_folder', type=Path, required=True,
                        help='folder containing extracted 10X data (AIRR files and hashtag data)')
    parser.add_argument('-o', dest='output_folder', type=Path, required=True,
                        help='all output goes here')
    parser.add_argument('-t', '--datatype', required=True, choices=["BCR", "TCR"],
                        help='data type')
    parser.add_argument('-p', '--feature-pattern', default='_5DE',
                        help='pattern to identify the feature folders')
    parser.add_argument('-c', '--hashtag-col-pattern', default='Hashtag',
                        help='pattern to identify the hashtag columns')
    parser.add_argument('-s', '--show-plot', action='store_true',
                        help='show gaussian fit')

    args = parser.parse_args()

    if not args.output_folder.is_dir():
        args.output_folder.mkdir(parents=True)

    for feature_dir in args.input_folder.glob('*'):
        if not feature_dir.is_dir():
            continue

        if feature_dir.name.endswith(args.feature_pattern):
            dataset_id = feature_dir.name.replace(args.feature_pattern, "")

            airr_folder = feature_dir.parent / f'{dataset_id}_{args.datatype}'
            if not airr_folder.is_dir():
                print(f'ERROR: AIRR folder {airr_folder} not found')
                continue

            airr_file = airr_folder / "outs" / "airr_rearrangement.tsv"
            if not airr_file.is_file():
                print(f'ERROR: AIRR file {airr_file} not found')
                continue

            dhash = feature_dir / "outs" / "filtered_feature_bc_matrix"
            if not dhash.is_dir():
                print(f'ERROR: feature folder {dhash} not found')
                continue

            # add hashtags to AIRR data
            hashtag_data = extract_hashtags(dhash)
            airr = pd.read_table(airr_file)
            merged = pd.merge(airr, hashtag_data, how ='inner', on='cell_id')
            out_file = args.output_folder / f'{dataset_id}.airr_rearrangement_hashassigned.tsv'
            #assign_hashtags(merged, out_file, args.hashtag_col_pattern)
            threshold= get_hashtag_threshold(merged, out_file, args.hashtag_col_pattern, args.show_plot)

            print(threshold,pow(2, threshold))
            assign_multi_hashtags(merged, out_file, threshold, args.hashtag_col_pattern)
                                             
