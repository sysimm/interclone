#!/usr/bin/env python3
# coding: utf-8
"""InterClone data preparation."""
import argparse
import logging
import shutil
import subprocess
import sys
import tempfile
from collections import defaultdict
from functools import partial
from multiprocessing import Pool, log_to_stderr
from pathlib import Path

import pandas as pd
from Bio import SeqIO

from common import update_progress, write_progress
from common import BASE_DIR, RUNNERS


def split_full_fa(raw_full_fa, run_dir):
    """Split a fasta file into smaller files."""
    sequences = list(SeqIO.parse(raw_full_fa, 'fasta'))
    seq_count = len(sequences)
    if seq_count < 100:
        split_count = 16
    else:
        split_count = 64  # always same number to ensure consistent runtimes

    if seq_count < split_count:
        split_count = 1
        batch_size = seq_count
    else:
        batch_size = int(seq_count / split_count) + 1

    logging.debug('Total seq len: %s', seq_count)
    logging.debug('Each file seq count: %s', batch_size)

    offset = 0
    for i in range(split_count):

        start, end = offset, offset + batch_size
        seq_batch = sequences[start:end]
        part_fasta = run_dir / f'part-{i}.fa'
        SeqIO.write(seq_batch, part_fasta, 'fasta')
        offset += batch_size

    return split_count, seq_count


def batchrun_pseudo_lsf(run_dir, split_num, chain_mode, species, threads, parallel):
    """Run Anarci in batch via sysimm runner."""
    template = BASE_DIR / 'templates' / 'anarci_cdrs.lsf'
    annotate_indices = run_dir / 'annotate_indices'
    with open(annotate_indices, 'wt') as indices_file:
        for i in range(split_num):
            indices_file.write(str(i) + '\n')

    annotate_cmd = [
        RUNNERS[parallel],
        '-jobs', '32',
        '-template', str(template),
        '-args', str(BASE_DIR), run_dir, chain_mode, species, str(threads), str(annotate_indices)
    ]
    logging.debug(annotate_cmd)
    log_file = run_dir / 'sysimm_runner.log'
    with open(log_file, 'wt') as log_handle:
        subprocess.run(annotate_cmd, check=True, stdout=log_handle)


def run_annotate_anarci(run_dir, chain_mode, species, threads, idx):
    """Run Anarci for a single input file."""
    log_file = run_dir / f'part-{idx}_pseudo.log'
    annotate_cmd = [
        BASE_DIR / 'src' / 'anarci_cdrs.py',
        '--input', run_dir / f'part-{idx}.fa',
        '--out', run_dir / f'part-{idx}_pseudo.fa',
        '--chain_type', chain_mode,
        '--ncpu', str(threads),
    ]
    if species and species != 'all':
        annotate_cmd.extend([
            '--species', species,
        ])

    logging.debug(annotate_cmd)
    with open(log_file, 'wt') as log_handle:
        subprocess.run(annotate_cmd, check=True, stdout=log_handle)


def merge_pseudo_anarci(run_dir, out_handle, split_num):
    """Combine batch anarci results."""
    counter = 0
    for i in range(split_num):
        part_pseudo_file = run_dir / f'part-{i}_pseudo.fa'
        try:
            with open(part_pseudo_file, 'r') as part_handle:
                for line in part_handle:
                    out_handle.write(line)
                    counter += 1
        except FileNotFoundError:
            logging.warning("can\'t find %s", part_pseudo_file)
    return counter


def make_pseudo_seq_anarci(chain, file_list, species, parallel, threads, anarci_threads, progress_path):
    """Set up Anarci in batch run."""
    for full_fasta in file_list:
        if not full_fasta.is_file():
            continue

        pseudo_fasta_out = full_fasta.parent / (full_fasta.stem + '-pseudo.fa')
        if pseudo_fasta_out.is_file():
            seq_count = sum(1 for _ in SeqIO.parse(pseudo_fasta_out, 'fasta'))
        else:
            part_fasta_subdir = full_fasta.parent / (full_fasta.stem + '-split-fasta')
            part_fasta_subdir.mkdir(parents=True, exist_ok=True)

            split_num, seq_count = split_full_fa(full_fasta, part_fasta_subdir)

            if parallel == 'local':
                if threads > 1:
                    pool = Pool(processes=threads)
                    log_to_stderr()
                    partial_run_annotate = partial(run_annotate_anarci, part_fasta_subdir, chain,
                                                   species, anarci_threads)
                    pool.map(partial_run_annotate, range(split_num))
                else:
                    for idx in range(split_num):
                        run_annotate_anarci(part_fasta_subdir, chain, species, anarci_threads, idx)
            else:
                batchrun_pseudo_lsf(part_fasta_subdir, split_num, chain, species, anarci_threads, parallel)

            with open(pseudo_fasta_out, "w") as pfh:
                merge_pseudo_anarci(part_fasta_subdir, pfh, split_num)

        update_progress(progress_path, seq_count)


def generate_features(file_list, dataset_name, subgroup_column, tag, run_folder, merge, no_index):
    """Extract CDR features from fasta output."""
    feature_names = ['ncdr1', 'ncdr2', 'ncdr3', 'acdr1', 'acdr2', 'acdr3']
    mmseqs_results = []

    for full_fasta in file_list:
        if not full_fasta.is_file():
            continue

        output_file = full_fasta.with_suffix('.tsv')
        original_data = pd.read_table(output_file, index_col='sequence_id', dtype=str)

        pseudo_fasta_file = full_fasta.parent / (full_fasta.stem + '-pseudo.fa')
        pseudo_fasta_file_joined = full_fasta.parent / (full_fasta.stem + '-pseudo_joined.fa')

        with open(pseudo_fasta_file_joined, "w") as fastaout:
            features = {}
            for record in SeqIO.parse(pseudo_fasta_file, 'fasta'):
                pseudo_seq = str(record.seq)
                parts = pseudo_seq.split("_")
                if len(parts) != 3 or any(len(part) == 0 for part in parts):
                    continue

                cdr1, cdr2, cdr3 = parts
                ncdr1 = len(cdr1)
                ncdr2 = len(cdr2)
                ncdr3 = len(cdr3)

                fastaout.write(f">{record.name}_{ncdr1}_{ncdr2}_{ncdr3}\n{cdr1}{cdr2}{cdr3}\n")

                record_feat = [ncdr1, ncdr2, ncdr3, cdr1, cdr2, cdr3]
                features[record.name] = record_feat

        if not features:
            continue

        features_df = pd.DataFrame.from_dict(features, orient='index', columns=feature_names)

        defaults = {'ncdr1': 0, 'ncdr2': 0, 'ncdr3': 0}
        merged = original_data.join(features_df).fillna(defaults).convert_dtypes()

        # add metadata for cluster comparison
        merged['dataset_name'] = dataset_name
        if subgroup_column and subgroup_column in merged.columns:
            merged['dataset_subgroup'] = merged[subgroup_column]
        else:
            # use filename as fallback, implying one donor per file
            merged['dataset_subgroup'] = full_fasta.stem

        if tag:
            if 'class' in merged.columns:
                # back up existing column
                merged.rename(columns={'class': 'author_class'})
            merged['class'] = tag

        # uniq_index = pd.read_table(full_fasta.with_suffix('.idx'), index_col=sequence_column)

        # update sequence counts for representatives
        # count_column = sequence_column + '_count'
        # merged.loc[uniq_index['sequence_id'], count_column] = uniq_index['count'].values
        # propagate features & counts to entries with same sequence (according to index table)
        # incomplete_mask = merged[['acdr1', 'acdr2', 'acdr3']].isna().any(axis=1) \
        #                 & merged[sequence_column].isin(uniq_index.index)
        # incomplete = merged[incomplete_mask]
        # refs = uniq_index.reindex(incomplete[sequence_column])

        # merged.loc[incomplete_mask, count_column] = refs['count'].values
        # missing_features = merged.loc[refs['sequence_id'], feature_names]
        # merged.loc[incomplete_mask, feature_names] = missing_features.values

        merged.to_csv(output_file, sep='\t')

        mmseqs_db_folder = full_fasta.parent / (full_fasta.stem + '-mmseqs')
        mmseqs_success = create_mmseqs_db(pseudo_fasta_file_joined, mmseqs_db_folder, no_index)
        if mmseqs_success:
            mmseqs_results.append(mmseqs_db_folder)
        else:
            logging.warning("unable to create mmseqs database %s", mmseqs_db_folder)

    if merge and mmseqs_results:
        merged_db = run_folder / 'merged' / 'seq.db'
        merged_db.parent.mkdir(parents=True, exist_ok=True)

        db_files = [db_folder / 'seq.db' for db_folder in mmseqs_results]
        merge_cmd = [
            'mmseqs',
            'mergedbs',
            db_files[0],
            merged_db
        ] + db_files[1:]
        subprocess.run(merge_cmd, check=True)

        merged_header_db = run_folder / 'merged' / 'seq.db_h'
        db_header_files = [db_folder / 'seq.db_h' for db_folder in mmseqs_results]
        merge_header_cmd = [
            'mmseqs',
            'mergedbs',
            db_header_files[0],
            merged_header_db
        ] + db_header_files[1:]
        subprocess.run(merge_header_cmd, check=True)


def create_mmseqs_db(sequences_fasta, output_folder, no_index):
    """ Create mmseqs database and index for faster searching and clustering """
    seq_db_index = output_folder / "seq.db.idx"
    if output_folder.is_dir():
        return seq_db_index.is_file()

    output_folder.mkdir(parents=True)

    seq_db = output_folder / "seq.db"
    cmd = ["mmseqs", "createdb", sequences_fasta, seq_db, "--dbtype", "1"]
    log_file = output_folder / 'mmseqs.log'
    with open(log_file, 'wt') as log_handle:
        subprocess.run(cmd, check=True, stdout=log_handle)

    if not seq_db.is_file():
        return False

    if no_index:
        return True

    tempdir = tempfile.mkdtemp()
    cmd = ["mmseqs", "createindex", seq_db, tempdir, "--threads", "8"]
    with open(log_file, 'at') as log_handle:
        subprocess.run(cmd, check=True, stdout=log_handle)
    shutil.rmtree(tempdir)

    return seq_db_index.is_file()


def seq_from_tsv(file_index, input_file, dataset_name, gene_names, output_file, sequence_column,
                 v_gene_column, seq_id_column):
    """TSV/AIRR sequence parser."""
    if output_file.is_file():
        logging.debug('reusing existing sequence file %s', output_file)
        return

    logging.debug('extracting sequences from %s', input_file)

    airr = pd.read_table(input_file)

    if airr.empty:
        logging.warning('No data found in input file %s', input_file)
        return

    if set([sequence_column, seq_id_column]).difference(airr.columns):
        logging.warning('Unable to find required data columns: %s, %s in input file %s',
                        sequence_column, seq_id_column, input_file)
        return

    if 'sequence_id' in airr.columns:
        # backup original data because we will overwrite it
        airr['author_sequence_id'] = airr['sequence_id']
        if seq_id_column == 'sequence_id':
            seq_id_column = 'author_sequence_id'

    airr['sequence_id'] = f'{dataset_name}_{file_index}_' + airr.index.astype(str)

    # save global sequence ID for later reference
    airr.to_csv(input_file, sep='\t', index=False)

    mask = airr[sequence_column].str.len() < 50
    if v_gene_column in airr.columns:
        # merge separate masks for the allowed genes
        first_gene, *other_genes = gene_names
        gene_mask = airr[v_gene_column].str.contains(first_gene)
        for other_gene in other_genes:
            # allow any of the genes by OR'ing the masks
            gene_mask |= airr[v_gene_column].str.contains(other_gene)

        # invert gene mask to filter out unwanted sequences
        mask |= ~gene_mask
    else:
        logging.info('V gene column %s not found in file %s. Data was not filtered.', v_gene_column, input_file)

    filtered = airr[~mask]
    if filtered.empty:
        logging.warning('no valid sequences found in %s', input_file)
        return

    fasta = '>' + filtered['sequence_id'] + ' ' + filtered[seq_id_column].astype(str) + '\n' + filtered[sequence_column].str.upper()

    # # compute frequency of each unique aa seq and assign to column sequence_column + '_count'
    # # initialize counts to zero to cover unfiltered data
    # airr[sequence_column + '_count'] = 0

    # # de-duplicate sequences
    # uniq_seqs = filtered[sequence_column].drop_duplicates() # preserves index
    # seq_counts = filtered[sequence_column].value_counts() # removes index
    # seq_counts.name = 'count'
    # seq_index = input_file.with_suffix('.idx')

    # # merge indices with global seq IDs and group sizes
    # uniq_stats = pd.DataFrame(uniq_seqs) \
    #                 .join(filtered['sequence_id']) \
    #                 .merge(seq_counts, left_on=sequence_column, right_index=True)
    # uniq_stats.to_csv(seq_index, sep='\t', index=False)

    # # write unique sequences to fasta
    # uniq_data = filtered.loc[uniq_seqs.index]
    # uniq_fastas = '>' + uniq_data['sequence_id'].astype(str) + ' ' + uniq_data[seq_id_column].astype(str) \
    #               + '\n' + uniq_data[sequence_column]

    # if uniq_fastas.size == 0:
    if fasta.empty:
        logging.warning('No sequences extracted from input file %s', input_file)
        return

    with open(output_file, "w") as outfh:
        outfh.write(fasta.str.cat(sep='\n'))
        # outfh.write('\n'.join(uniq_fastas))
        outfh.write('\n')

    # save global sequence ID and unique sequence ID for later reference
    airr.to_csv(input_file, sep='\t', index=False)

    logging.debug('finished %s', input_file)


def extract_sequences(input_files, dataset_name, aa_seq_column, v_gene_column, seq_id_column):
    """Wrapper for TSV/AIRR sequence parser."""
    chain_to_genes = {
        'H': ['IGH'],
        'L': ['IGL', 'IGK'],
        'A': ['TRA'],
        'B': ['TRB'],
    }

    sequence_files = defaultdict(list)
    for chain in input_files:
        file_id = 0
        for input_file in input_files[chain]:
            out_file = input_file.with_suffix('.fa')
            try:
                seq_from_tsv(file_id, input_file, dataset_name, chain_to_genes[chain],
                             out_file, aa_seq_column, v_gene_column, seq_id_column)
            except Exception:
                logging.exception('Unable to read input file %s', input_file)
                continue

            sequence_files[chain].append(out_file)
            file_id += 1

    return sequence_files


def cleanup_batch_files(run_folder):
    """Temp file removal."""
    for split_dir in run_folder.glob('**/*-split-fasta'):
        shutil.rmtree(split_dir)


def main():
    """Prepare InterClone datasets from TSV inputs."""
    anarci_species = ['human', 'mouse', 'rat', 'rabbit', 'rhesus', 'pig', 'alpaca', 'cow', 'all']
    parser = argparse.ArgumentParser(description='make sequence features from BCRs')
    parser.add_argument('-i', dest='inputs', type=Path, required=True,
                        help='folder containing AIRR files or single AIRR file')
    parser.add_argument('-o', dest='run_folder', type=Path, required=True,
                        help='all output goes here')
    parser.add_argument('--name', required=True, help='short name for the dataset')
    parser.add_argument('--subgroup', help='column name for data subgroups (default: file name)')
    parser.add_argument('--tag', help='additional text to append to subgroup name (default: nothing)')
    parser.add_argument('-c', '--chain', default="H",
                        choices=["H", "L", "B", "A", "HL", "AB"], help='chain')
    parser.add_argument('--species', choices=anarci_species, default='all',
                        help='which species to consider (default: all)')
    parser.add_argument('--seq-id-column', default='sequence_id',
                        help='TSV column containing the sequence identifier')
    parser.add_argument('--aa-seq-column', default='sequence_aa',
                        help='TSV column containing the full AA sequence')
    parser.add_argument('--v-gene-column', default='v_call',
                        help='TSV column containing the V gene name (optional)')
    parser.add_argument('--merge', action='store_true', help='merge per-file mmseqs databases')
    parser.add_argument('--keep-temp', action='store_true', help='do not delete temporary files')
    parser.add_argument('--threads', default=1, type=int, help='number of parallel threads to run')
    parser.add_argument('--anarci-threads', default=8, type=int,
                        help='number of parallel threads to run Anarci')
    parser.add_argument('--parallel', default='local', choices=['local', 'lsf', 'slurm'],
                        help='method of parallelization')
    parser.add_argument('--no-index', action='store_true',
                        help='skip index creation of mmseqs database')
    parser.add_argument('-v', '--verbose', action='store_true', help='print debug output')

    args = parser.parse_args()

    if not (args.inputs.is_dir() or args.inputs.suffix == '.tsv'):
        raise Exception("inputs not found")

    # check if parallelization method works
    if args.parallel == 'lsf' and not shutil.which('bsub'):
        logging.error('Unable to use LSF, please choose another parallelization option')
        sys.exit(1)
    elif args.parallel == 'slurm' and not shutil.which('sbatch'):
        logging.error('Unable to use Slurm, please choose another parallelization option')
        sys.exit(1)

    if not args.verbose:
        logging.getLogger().level = logging.INFO

    args.run_folder.mkdir(exist_ok=True, parents=True)

    progress_data = {
        'totalSteps': 0,
        'currentStep': 0,
        'numSeq': 0,
        'status': 'pending',
    }
    progress_path = args.run_folder / "progress.json"
    write_progress(progress_data, progress_path)

    if args.inputs.is_dir():
        input_files = list(args.inputs.glob('**/*.tsv'))
        if not input_files:
            update_progress(progress_path, status='error')
            raise Exception(f'No input files found inside {args.inputs}')
    else:
        input_files = [args.inputs]

    #initializing and finalizing
    extra_steps = 2
    progress_data['totalSteps'] = len(input_files) * len(args.chain) + extra_steps
    progress_data['status'] = 'running'

    write_progress(progress_data, progress_path)

    try:
        # preserve directory structure in output folder
        output_files = defaultdict(list)
        for chain in args.chain:
            for input_file in input_files:
                if input_file.name.startswith('._'):
                    # Mac OS system files, ignore
                    continue
                rel_path = input_file.relative_to(args.inputs).parent
                target = args.run_folder / chain / rel_path
                target.mkdir(exist_ok=True, parents=True)
                logging.debug('copying %s to %s', input_file, target)
                shutil.copy(input_file, target)
                output_files[chain].append(target / input_file.name)

        # extract sequences and indices; write as fasta
        indexed_files = extract_sequences(output_files, args.name, args.aa_seq_column,
                                          args.v_gene_column, args.seq_id_column)

        if indexed_files:
            update_progress(progress_path)
        else:
            raise Exception('Unable to process inputs')

        for chain in indexed_files:
            # define cdrs and alignment positions
            make_pseudo_seq_anarci(chain, indexed_files[chain], args.species, args.parallel,
                                   args.threads, args.anarci_threads, progress_path)

            # construct a tsv file containing cdr lengths and sequences
            generate_features(indexed_files[chain], args.name, args.subgroup, args.tag,
                              args.run_folder, args.merge, args.no_index)

        if not args.keep_temp:
            cleanup_batch_files(args.run_folder)

        update_progress(progress_path)
        update_progress(progress_path, status='finished')

    except Exception as ex:
        update_progress(progress_path, status='error')
        raise Exception('Unable to prepare pseudo sequences') from ex


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.DEBUG,
        format='[%(asctime)s] %(levelname)s in %(module)s-%(lineno)d: %(message)s'
    )
    main()
