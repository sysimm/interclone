#!/usr/bin/env python
# coding: utf-8
import argparse
import sys
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pandas as pd


# Parsing all arguments
parser = argparse.ArgumentParser(description='calculate ave or median')
parser.add_argument("-i", dest='fin', required=True, help="input file")
parser.add_argument("-o", dest='fout', help="output file")
parser.add_argument("-c", dest='col', default="Hashtag", help="input column")
parser.add_argument("-p", dest='plot', action='store_true', help="plot data")
parser.add_argument("-t", dest='threshold', default=6.0,
                    type=float, help="read count threshold in log2")
args = parser.parse_args()


if not args.plot and not args.fout:
    raise Exception("you must use either the -p or the -o flag")

df = pd.read_table(args.fin, sep="\t")

use_col = [] #set(df.columns).intersection(args.col)
for df_col in df.columns:
    if args.col in df_col:
        use_col.append(df_col)

x = []
y = []
for c in use_col:
    x1 = df[c].values
    y1 = np.log2(x1[x1 > 0])

    x.append(x1)
    y.append(y1)


if args.plot:
    colors = cm.rainbow(np.linspace(0, 1, len(x)))

    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i in range(0, len(x)):
        ax.hist(y[i], bins=30, alpha=0.5, lw=3, color=colors[i], label=use_col[i])

    ax.set_xlabel('log2(read count)')
    ax.set_ylabel('Frequency')
    ax.set_title(os.path.basename(args.fin))
    plt.legend(loc='upper right')
    plt.axvline(x=args.threshold)
    plt.show()
    sys.exit()


my_read_cut = pow(2, args.threshold)
df['AssignedTag'] = "UNK"

for index, row in df.iterrows():
    c_best = use_col[0]
    v_best = 0
    for c in use_col:
        if int(row[c]) > v_best:
            c_best = c
            v_best = int(row[c])

    if v_best >= my_read_cut:
        df.loc[index, 'AssignedTag'] = c_best
    else:
        print("failed to assign row ", index)


df.to_csv(args.fout, sep='\t', index=False)
