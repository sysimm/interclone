#!/usr/bin/env python3

""" Convert a FASTA input file into proper AIRR format for the InterClone pipeline """

import argparse
import contextlib
import csv
import sys
from Bio import SeqIO


@contextlib.contextmanager
def smart_open(filename=None):
    """ replacement for the open() function that can handle stdout """

    if filename and filename != '-':
        handle = open(filename, 'w', newline='')
    else:
        handle = sys.stdout

    try:
        yield handle
    finally:
        if handle is not sys.stdout:
            handle.close()


parser = argparse.ArgumentParser('Converts FASTA input to AIRR output')
parser.add_argument('-i', '--inputs', required=True, nargs='+',
                    help='paths to one or more FASTA input files')
parser.add_argument('-c', '--chain', choices=["IGH", "IGL", "IGK", "TRB", "TRA"],
                    required=True, help='chain')
parser.add_argument('-o', '--output', help='output file (default stdout)')

args = parser.parse_args()

idx = 0
with smart_open(args.output) as out_handle:
    airr_writer = csv.writer(out_handle, delimiter='\t')
    header = ['sequence_id', 'v_call', 'sequence_aa']
    airr_writer.writerow(header)

    for input_file in args.inputs:
        for record in SeqIO.parse(input_file, 'fasta'):
            row = [f'{idx}_{record.name}', args.chain, str(record.seq)]
            airr_writer.writerow(row)
            idx += 1
