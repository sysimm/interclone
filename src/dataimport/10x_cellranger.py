#!/usr/bin/env python

"""
Creates AIRR formatted TSV files for the Interclone pipeline from raw 10X
CellRanger output. Combines sequence data from the Rearrangement file,
clonotype mapping from the contig annotations file and clonotype frequencies
from the clonotypes file.
"""

import argparse
import sys
from pathlib import Path
import pandas as pd


parser = argparse.ArgumentParser('Convert 10X CellRanger output to AIRR file')
parser.add_argument('-i', '--inputs', required=True, type=Path, nargs='+',
                    help='paths to one or more CellRanger output folders')
parser.add_argument('-n', '--name', required=True, help='dataset name')
parser.add_argument('-o', '--out', type=Path, required=True, help='output folder')
parser.add_argument('-c', '--chain', choices=['TCR', 'BCR'], required=True, help='chain type')
args = parser.parse_args()

args.out.mkdir(parents=True, exist_ok=True)

out_cols = ['cell_id', 'clone_id', 'sequence_id', 'sequence_aa', 'v_call', 'j_call', 'cdr3', 'c_call', 'clone_count']
results = []
for file_index, input_folder in enumerate(args.inputs):
    rearrangement_file = input_folder / 'airr_rearrangement.tsv'
    clonotypes_file = input_folder / 'clonotypes.csv'
    annotations_file = input_folder / 'all_contig_annotations.csv'

    if not rearrangement_file.is_file():
        print(f'ERROR: rearrangement file {rearrangement_file} not found')
        sys.exit()

    if not clonotypes_file.is_file():
        print(f'ERROR: clonotypes file {clonotypes_file} not found')
        sys.exit()

    if not annotations_file.is_file():
        print(f'ERROR: contigs annotations file {annotations_file} not found')
        sys.exit()

    clonotypes = pd.read_csv(clonotypes_file, usecols=['cdr3s_aa', 'clonotype_id', 'frequency'])
    cdr3s_aa = clonotypes['cdr3s_aa']

    if args.chain == 'TCR':
        paired_mask = cdr3s_aa.str.contains('TRA:') & cdr3s_aa.str.contains('TRB:')
    else:
        # two possible light chains
        paired_mask = cdr3s_aa.str.contains('IGH:') & \
                      (cdr3s_aa.str.contains('IGL:') | cdr3s_aa.str.contains('IGK:'))

    # make sure there are two CDR3 sequences separated by one semicolon
    paired_mask &= cdr3s_aa.str.count(';') == 1
    paired_clonotypes = clonotypes.loc[paired_mask]
    frequencies = paired_clonotypes[['clonotype_id', 'frequency']].set_index('clonotype_id')

    # multiple contigs may have the same barcode/clonotype mapping
    barcode2clonotype = pd.read_csv(annotations_file, usecols=['barcode', 'raw_clonotype_id'])
    barcode2clonotype.drop_duplicates(inplace=True)

    rearrangement = pd.read_table(rearrangement_file, usecols=['cell_id', 'v_call', 'j_call', 'c_call', 'sequence_aa', 'junction_aa'])
    rearrangement['short_v'] = rearrangement['v_call'].str.slice(0, 3)

    # use only the first contig, identified by clone ID and V gene
    merged = rearrangement.merge(barcode2clonotype, left_on='cell_id', right_on='barcode') \
                          .merge(frequencies, left_on='raw_clonotype_id', right_index=True) \
                          .groupby(['raw_clonotype_id', 'short_v']).first() \
                          .reset_index() \
                          .fillna('')

    # create unique IDs
    merged['cell_id'] = f'{file_index}_' + merged['raw_clonotype_id']
    merged['sequence_id'] = merged['cell_id'] + '_' + merged['short_v']

    # fix column names
    merged.rename({
        'raw_clonotype_id': 'clone_id',
        'frequency': 'clone_count',
        'junction_aa': 'cdr3'
    }, axis='columns', inplace=True)
    results.append(merged[out_cols])


out_file = args.out / f'{args.name}.tsv'
combined = pd.concat(results)

if combined.empty:
    print('ERROR: no data found')
else:
    combined.to_csv(out_file, sep='\t', index=False)
