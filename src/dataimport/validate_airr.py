#!/usr/bin/env python

# Validate and optionally fix InterClone input files

import argparse
import sys
import pandas as pd


def replace_column(columns, old_column, new_column):
    pos = columns.index(old_column)
    columns[pos] = new_column


def validate(columns, required_col_name, replacement_col):
    if required_col_name not in columns:
        if replacement_col:
            if replacement_col not in columns:
                print(f'Replacement column {replacement_col} not found')
                return False
            replace_column(columns, replacement_col, required_col_name)
        else:
            print(f'Required column "{required_col_name}" not found')
            return False
    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Validate and fix InterCloneDB input files')
    parser.add_argument('input_file', help='input TSV file')
    parser.add_argument('-o', dest='output_file', help='output fixed AIRR file')
    parser.add_argument('--seq-id-column',
                        help='new name for the TSV column for the sequence identifier')
    parser.add_argument('--aa-seq-column',
                        help='new name for the TSV column containing the full AA sequence')
    parser.add_argument('--v-gene-column',
                        help='new name for the TSV column containing the V gene name')
    args = parser.parse_args()

    try:
        airr = pd.read_table(args.input_file)
    except Exception as ex:
        print('Unable to read input file', ex)
        sys.exit(1)

    if airr.empty:
        print('No data found in input file',)
        sys.exit(1)

    with open(args.input_file) as handle:
        header = next(handle)
        columns = header.strip().split('\t')

    if not validate(columns, 'sequence_id', args.seq_id_column):
        sys.exit(1)
    if not validate(columns, 'sequence_aa', args.aa_seq_column):
        sys.exit(1)
    if not validate(columns, 'v_call', args.v_gene_column):
        pass # optional column

    if columns != airr.columns.tolist():
        if args.output_file:
            airr.columns = columns
            # write manually to preserve input data
            with open(args.input_file) as in_handle, \
                 open(args.output_file, 'wt') as out_handle:

                out_handle.write('\t'.join(columns) + '\n')
                next(in_handle) # skip header
                for line in in_handle:
                    out_handle.write(line)
        else:
            print('Replaced column names but no output files given')
    else:
        print('OK')
