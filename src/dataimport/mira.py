#!/usr/bin/env python

"""
Creates AIRR formatted TSV files for the Interclone pipeline from raw Illumina
MIRA output. Combines sequence data with metadata.
"""

import argparse
import sys
from pathlib import Path
import pandas as pd
from Bio import SeqIO


parser = argparse.ArgumentParser('Convert MIRA output to AIRR file')
parser.add_argument('--fasta', required=True, type=Path, help='path to sequence FASTA file')
parser.add_argument('--meta', required=True, type=Path, help='path to meta TSV file')
parser.add_argument('-n', '--name', required=True, help='dataset name')
parser.add_argument('-o', '--out', type=Path, required=True, help='output folder')
parser.add_argument('--v-gene-column', help='V gene column name')
args = parser.parse_args()

args.out.mkdir(parents=True, exist_ok=True)

mira_meta = pd.read_table(args.meta)
mira_fasta = SeqIO.to_dict(SeqIO.parse(args.fasta, 'fasta'))
mira_seqs = pd.Series(mira_fasta, name='sequence_aa').apply(''.join)

merged = mira_meta.merge(mira_seqs, left_on='source.id', right_index=True)
if 'sequence_id' not in merged:
    merged['sequence_id'] = merged.index

if 'v_call' not in merged and not args.v_gene_column:
    print('ERROR: could not determine V gene. Please provide --v-gene-column option')
    sys.exit(1)

merged['v_call'] = merged[args.v_gene_column]

out_file = args.out / f'{args.name}.tsv'
if merged.empty:
    print('ERROR: no data found')
else:
    merged.to_csv(out_file, sep='\t', index=False)
    print(f'wrote {len(merged)} of {len(mira_meta)} records')
