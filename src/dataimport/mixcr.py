#!/usr/bin/env python

"""
Creates AIRR formatted TSV files for the Interclone pipeline from raw MiXCR
output. Combines sequence data from the imputed CDR and framework regions and
discards sequences containing gaps or stop codons, with the exception of a
final gap at the end of framework 4.
"""

import argparse
from pathlib import Path
import pandas as pd


parser = argparse.ArgumentParser('Convert MiXCR output to AIRR file')
parser.add_argument('-i', '--inputs', required=True, type=Path, nargs='+',
                    help='paths to one or more MiXCR output files')
parser.add_argument('-n', '--name', required=True, help='dataset name')
parser.add_argument('-o', '--out', type=Path, required=True, help='output folder')
parser.add_argument('--include', nargs='+', default=[], help='additional columns to include')
args = parser.parse_args()

args.out.mkdir(parents=True, exist_ok=True)

in_cols = [
    'cloneId', 'cloneCount', 'allVHitsWithScore', 'allJHitsWithScore', 'allCHitsWithScore',
    'aaSeqImputedFR1', 'aaSeqImputedCDR1', 'aaSeqImputedFR2', 'aaSeqImputedCDR2',
    'aaSeqImputedFR3', 'aaSeqImputedCDR3', 'aaSeqImputedFR4'
]
sequence_cols = in_cols[5:]
out_cols = ['cell_id', 'clone_id', 'sequence_id', 'sequence_aa', 'v_call', 'j_call', 'cdr3', 'c_call', 'clone_count'] + args.include

results = []
for file_index, input_file in enumerate(args.inputs):
    if not input_file.is_file() or not input_file.resolve().is_file():
        print(f'ERROR: input file {input_file} not found')
        continue

    df = pd.read_table(input_file, usecols=in_cols+args.include, dtype=str)
    # combine sequence parts and remove final gap
    df['sequence_aa'] = df[sequence_cols].dropna().apply(''.join, axis=1).str.rstrip('_').str.upper()
    invalid_mask = df['sequence_aa'].isna() | \
                   df['sequence_aa'].str.contains('[_*]') | \
                   (df['sequence_aa'].str.len() < 50)
    df = df.loc[~invalid_mask].convert_dtypes()

    # extract gene names
    for gene in 'vjc':
        gene_hits = df[f'all{gene.upper()}HitsWithScore']
        if gene_hits.isna().all():
            df[f'{gene}_call'] = ''
        else:
            # use first entry if there are multiple
            df[f'{gene}_call'] = gene_hits.str.split('*', 1, expand=True).drop(1, axis=1).fillna('')

    # create unique IDs
    df['cell_id'] = f'{file_index}_' + df['cloneId'].astype(str)
    df['short_v'] = df['v_call'].str.slice(0, 3)
    df['sequence_id'] = df['cell_id'] + '_' + df['short_v']

    # fix column names
    df.rename({
        'cloneId': 'clone_id',
        'aaSeqImputedCDR3': 'cdr3',
        'cloneCount': 'clone_count'
    }, axis='columns', inplace=True)
    results.append(df[out_cols])


out_file = args.out / f'{args.name}.tsv'
combined = pd.concat(results)

if combined.empty:
    print('ERROR: no data found')
else:
    combined.to_csv(out_file, sep='\t', index=False)
