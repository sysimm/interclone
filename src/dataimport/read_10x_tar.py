#!/usr/bin/env python3
# coding: utf-8

# Extracts a number of files from 10X tar files.

import argparse
import tarfile
from pathlib import Path


def filtered_files(members, filters):
    for tarinfo in members:
        if any(f in tarinfo.name for f in filters):
            print(tarinfo.name)
            yield tarinfo


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='get files of interest from tar archive')
    parser.add_argument('-i', '--infile', required=True, type=Path,
                        help='tar file containing 10x data')
    parser.add_argument('-o', '--out', type=Path, help='output folder')
    parser.add_argument('-f', '--filter', nargs='+', help='filter to extract files')

    args = parser.parse_args()

    if not args.infile.is_file():
        raise Exception("input tar not found")

    out_dir = args.out.resolve()
    if out_dir.is_dir():
        print(f'warning: using existing output folder {out_dir}')

    out_dir.mkdir(parents=True, exist_ok=True)

    with tarfile.open(args.infile) as tar:
        tar.extractall(out_dir, members=filtered_files(tar, args.filter))
