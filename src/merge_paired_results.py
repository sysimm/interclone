#!/usr/bin/env python

# Merges expanded search results from two runs (heavy & light or alpha & beta) by a given column
# which identifies sequence pairs, e.g. clone_id. The output file is ordered so that the matched
# results should appear close together. Note that there aren't necessarily two entries per cell
# because either chain could have multiple hits. Entries are grouped by a new column "group_id".

import argparse
import gc
import logging
import sys
from collections import defaultdict
from pathlib import Path
import pandas as pd

from common import update_progress, write_progress


parser = argparse.ArgumentParser('merge paired search results')
parser.add_argument('--heavy', '--beta', type=Path, required=True, help='heavy/beta result TSV')
parser.add_argument('--light', '--alpha', type=Path, required=True, help='light/alpha result TSV')
parser.add_argument('-o', '--output', type=Path, required=True, help='output TSV file')
parser.add_argument('--compare-by', '--by', default='clone_id', help='column name to identify pairs')
parser.add_argument('--progress', type=Path, help='progress file path')
args = parser.parse_args()

progress_data = {
    'totalSteps': 1,
    'currentStep': 0,
    'status': 'running',
}
write_progress(progress_data, args.progress)

logging.basicConfig(level=logging.INFO)

# input validation
heavy = pd.read_table(args.heavy, dtype=str)
if args.compare_by in heavy.columns:
    heavy_group_col = args.compare_by
elif args.compare_by + '_query' in heavy.columns:
    heavy_group_col = args.compare_by + '_query'
else:
    logging.error('comparison column %s not found in file %s', args.compare_by, args.heavy)
    update_progress(args.progress, status='error')
    sys.exit()

if 'Query' not in heavy.columns or 'Templ' not in heavy.columns:
    logging.error('"Query"/"Templ" column not found in file %s. Did you provide an expanded results file?', args.heavy)
    update_progress(args.progress, status='error')
    sys.exit()

light = pd.read_table(args.light, dtype=str)
if args.compare_by in light.columns:
    light_group_col = args.compare_by
elif args.compare_by + '_query' in light.columns:
    light_group_col = args.compare_by + '_query'
else:
    logging.error('comparison column %s not found in file %s', args.compare_by, args.light)
    update_progress(args.progress, status='error')
    sys.exit()

if 'Query' not in light.columns or 'Templ' not in light.columns:
    logging.error('"Query"/"Templ" column not found in file %s. Did you provide an expanded results file?', args.light)
    update_progress(args.progress, status='error')
    sys.exit()

try:
    # split up Query and Template identifiers
    heavy[['query_name', 'query_file_idx', 'query_line_idx']] = heavy['Query'].str.rsplit('_', 2, expand=True)
    light[['query_name', 'query_file_idx', 'query_line_idx']] = light['Query'].str.rsplit('_', 2, expand=True)

    heavy[['templ_name', 'templ_file_idx', 'templ_line_idx']] = heavy['Templ'].str.rsplit('_', 2, expand=True)
    light[['templ_name', 'templ_file_idx', 'templ_line_idx']] = light['Templ'].str.rsplit('_', 2, expand=True)

    # group search results by query/template file combination because comparison values like clone_id
    # may only be unique per file
    matches = defaultdict(dict)
    heavy_grouped = heavy.groupby(['query_file_idx', 'templ_file_idx'])
    for heavy_idx, heavy_group in heavy_grouped:
        matches[heavy_idx]['heavy'] = set(heavy_group[heavy_group_col])

    light_grouped = light.groupby(['query_file_idx', 'templ_file_idx'])
    for light_idx, light_group in light_grouped:
        matches[light_idx]['light'] = set(light_group[light_group_col])

    match_count = sum(1 for heavy_light_data in matches.values() if len(heavy_light_data) == 2)
    progress_data['totalSteps'] = match_count
    write_progress(progress_data, args.progress)

    # keep those results that share pair identifiers and discard the rest
    matched_data = []
    group_idx = 0
    for (query_file_idx, templ_file_idx), heavy_light_data in matches.items():
        if len(heavy_light_data) != 2:
            continue

        common_entries = heavy_light_data['heavy'].intersection(heavy_light_data['light'])
        if not common_entries:
            continue

        update_progress(args.progress)
        for group_id in common_entries:
            heavy_mask = (heavy['query_file_idx'] == query_file_idx) & \
                        (heavy['templ_file_idx'] == templ_file_idx) & \
                        (heavy[heavy_group_col] == group_id)
            heavy.loc[heavy_mask, 'group_id'] = group_idx
            matched_data.append(heavy[heavy_mask])

            light_mask = (light['query_file_idx'] == query_file_idx) & \
                        (light['templ_file_idx'] == templ_file_idx) & \
                        (light[light_group_col] == group_id)
            light.loc[light_mask, 'group_id'] = group_idx
            matched_data.append(light[light_mask])

            group_idx += 1
            del heavy_mask, light_mask

        gc.collect()

    if matched_data:
        merged = pd.concat(matched_data).convert_dtypes()
        merged.sort_values(['group_id', 'query_file_idx', 'query_line_idx']) \
            .to_csv(args.output, sep='\t', index=False)
        logging.info('wrote %s matches to %s', len(merged), args.output)
    else:
        logging.info('No matches found')

except Exception:
    logging.exception('Unable to merge search results')
    update_progress(args.progress, status='error')
else:
    update_progress(args.progress, status='finished')
