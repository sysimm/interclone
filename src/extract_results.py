#!/usr/bin/env python

import argparse
import gc
import logging
import shutil
import sys
from itertools import chain
from pathlib import Path
import pandas as pd

from common import find_files_by_chain, update_progress, write_progress, ALLOWED_CHAINS


parser = argparse.ArgumentParser('extract search hits from input files')
parser.add_argument('-q', '--queries', nargs='+', type=Path, required=True,
                    help='folder(s) containing query AIRR files (should be same as in search call)')
parser.add_argument('-t', '--templates', nargs='+', type=Path, required=True,
                    help='folder(s) containing template AIRR files (should be same as in search call)')
parser.add_argument('-r', '--result', type=Path, required=True,
                    help='search result (hits) base folder')
parser.add_argument('-o', '--out', type=Path, required=True,
                    help='output folder path')
parser.add_argument('--clean', action="store_true",
                    help='remove output folder if it exists')
parser.add_argument('-c', '--chain', choices=ALLOWED_CHAINS,
                    help='chain(s) to process')
parser.add_argument('--progress', type=Path, help='progress file path')
args = parser.parse_args()

progress_data = {
    'totalSteps': 1,
    'currentStep': 0,
    'status': 'running',
}
write_progress(progress_data, args.progress)

logging.basicConfig(level=logging.INFO)

def load(filename):
    df = pd.read_table(filename, dtype=str)
    # remove empty or failed data (e.g. different chains)
    mask = (df.acdr1 == '0') | (df.acdr2 == '0') | (df.acdr3 == '0')
    return df[~mask]

query_files = find_files_by_chain(args.queries, args.chain, '*.tsv')
queries = {
    chain: pd.concat([load(query) for query in queries_chain])
        for (chain, queries_chain) in query_files.items()
}

result_files = find_files_by_chain(args.result, args.chain, 'searchhits.*', True)
results = {
    chain: pd.concat([pd.read_json(result) if result.suffix == '.json' else pd.read_table(result) for result in results_chain])
        for (chain, results_chain) in result_files.items()
}

templ_files = find_files_by_chain(args.templates, args.chain, '*.tsv')
templ_file_count = sum(len(tf) for tf in templ_files.values())

if set(queries) != set(results) or set(queries) != set(templ_files):
    logging.error('Query, template and result chains don\'t match')
    update_progress(args.progress, status='error')
    sys.exit(1)

if not queries:
    logging.error('No query data found!')
    update_progress(args.progress, status='error')
    sys.exit()
elif not results:
    logging.error('No result data found!')
    update_progress(args.progress, status='error')
    sys.exit()

if args.clean and args.out.is_dir():
    shutil.rmtree(args.out)

progress_data['totalSteps'] = templ_file_count
write_progress(progress_data, args.progress)

try:
    for chain, template_files in templ_files.items():
        (args.out / chain).mkdir(parents=True)
        chain_out = args.out / chain / 'meta.tsv'
        query = queries[chain]
        result = results[chain]

        if result.empty:
            continue

        parts = []
        for template_file in template_files:
            template = pd.read_table(template_file, dtype=str)
            templates_mask = (template.acdr1 == '0') | (template.acdr2 == '0') | (template.acdr3 == '0')
            template = template[~templates_mask]

            if template.empty:
                logging.warning('no template data found in %s', template_file)
                update_progress(args.progress)
                continue

            merged = result \
                        .merge(query, left_on='Query', right_on='sequence_id', suffixes=(None, '_query')) \
                        .merge(template, left_on='Templ', right_on='sequence_id', suffixes=('_query', '_template'))

            parts.append(merged)
            update_progress(args.progress, len(merged))
            del template, templates_mask
            gc.collect()

        merged = pd.concat(parts)
        result_count = len(merged)
        if result_count != len(result):
            logging.warning('extended result count differs from search hits')

        merged.to_csv(chain_out, sep='\t', index=None)
        logging.info('wrote %s entries for chain %s', result_count, chain)
        del merged, parts
        gc.collect()

except Exception:
    logging.exception('Unable to extract search results')
    update_progress(args.progress, status='error')
else:
    update_progress(args.progress, status='finished')
