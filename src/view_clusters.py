#!/usr/bin/env python3
# coding: utf-8
import argparse
from pathlib import Path
import pandas as pd
import plotly.express as px


parser = argparse.ArgumentParser('format json file')
parser.add_argument('-i', '--input-file', type=Path, required=True,
                    help='cluster json summary')
parser.add_argument('-s', '--sort-column', default='cluster_size',
                    help='column to sort by')
parser.add_argument('-n', '--num-clust', type=int, default=15,
                    help='number of clusters to print')
parser.add_argument("-p",'--make-plot',action="store_true",help="make plots")
parser.add_argument("-t", dest='title', default="InterClone Clusters",help="title")
parser.add_argument("-o", dest='fout', help="output file")
parser.add_argument("-log", dest='use_log', action='store_true', help="log scale for y axis")
args = parser.parse_args()

use_log = args.use_log

data_columns = ['representative', 'nmem', 'Cluster Size', 'ngroup', 'maxgroup', 'cdr1', 'cdr2', 'cdr3', 'v_call', 'j_call']
df = pd.read_table(args.input_file).sort_values(args.sort_column, ascending=False).rename(columns={'cluster_size': 'Cluster Size'})

top_data = df.iloc[:args.num_clust]
pd.options.display.width = 120
pd.options.display.expand_frame_repr = False
print(top_data)

if args.make_plot:
    named_colorscales = px.colors.named_colorscales()

    top_data["CDRs"] = top_data['cdr1'] + " " + top_data['cdr2'] + " " + top_data['cdr3']
    # top_data["Fraction of Sustainers"] = top_data['ngroup'] / top_data['Cluster Size']
    top_data["Fraction of Donors"] = top_data['donors_group'] / top_data['donors_all']

    fig = px.bar(top_data, x='CDRs', y='Cluster Size', color='Fraction of Donors', title=args.title,
                 color_continuous_scale='bluered', range_color=(0, 1))

    if use_log:
        fig.update_yaxes(type="log")

    if args.fout:
        fig.update_layout(width=2000, height=1000, font_size=20)
        fig.write_image(args.fout)
    else:
        fig.show()
