#!/usr/bin/env python

# this file is used as a sysimm runner template, hence the BSUB options

#SBATCH --ignore-pbs
#INPUT CONSTANT,base_dir
#INPUT CONSTANT,out_folder
#INPUT CONSTANT,sid1
#INPUT CONSTANT,sid2
#INPUT CONSTANT,sid3
#INPUT CONSTANT,cov
#INPUT CONSTANT,status_file
#INPUT VARIABLE,query_db
#INPUT VARIABLE,template_db
#INPUT VARIABLE,query_id
#INPUT VARIABLE,template_id
#INPUT VARIABLE,log_file
#BSUB -q interclone
#BSUB -J search_db
#BSUB -o {{.log_file}}
#SBATCH -p interclone
#SBATCH -J search_db
#SBATCH -o {{.log_file}}

import argparse
import csv
import sys
from pathlib import Path

sys.path.insert(0, "{{.base_dir}}") # tell LSF job about the project location
sys.path.insert(0, "{{.base_dir}}/src") # and enable imports in the src folder
from src.search_db import run_search, update_progress

try:
    parser = argparse.ArgumentParser('run a single mmseqs search template/query pair')
    parser.add_argument('-q', dest='query_db', default="{{.query_db}}", type=Path,
                        help='a folder containing a query mmseqs DB')
    parser.add_argument('-t', dest='templ_db', default="{{.template_db}}", type=Path,
                        help='a folder containing one or more DB folders')
    parser.add_argument('-o', dest='out_folder', default="{{.out_folder}}", type=Path,
                        help='output_folder')
    parser.add_argument('--qid', default="{{.query_id}}", help='query ID')
    parser.add_argument('--tid', default="{{.template_id}}", help='template ID')
    parser.add_argument('--sid1', dest='sid1', default="{{.sid1}}", type=int,
                        help='cdr1 % sequence identity cutoff [0-100]')
    parser.add_argument('--sid2', dest='sid2', default="{{.sid2}}", type=int,
                        help='cdr2 % sequence identity cutoff [0-100]')
    parser.add_argument('--sid3', dest='sid3', default="{{.sid3}}", type=int,
                        help='cdr3 % sequence identity cutoff [0-100]')
    parser.add_argument('--cov', dest='cov', default="{{.cov}}", type=int,
                        help='min % align coverage [0-100]')
    parser.add_argument('--status', dest='status_file', default="{{.status_file}}", type=Path,
                        help='parent job status file')

    args = parser.parse_args()

    progress_path = Path(args.status_file) if args.status_file else None
    query = (args.qid, args.query_db)
    template = (args.tid, args.templ_db)
    result, out_path = run_search(args.out_folder, args.cov, args.sid1, args.sid2, args.sid3,
                                  progress_path, None, query, template, return_path=True)
    with open(out_path / 'searchhits.tsv', 'wt', newline='') as result_handle:
        result_writer = csv.writer(result_handle, delimiter='\t', lineterminator='\n')
        result_writer.writerows(result)

except Exception as ex:
    update_progress(progress_path, status='error')
    raise Exception('Unable to perform search via LSF') from ex
