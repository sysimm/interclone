#!/usr/bin/env python

import argparse
import gc
import json
import logging
import shutil
import sys
from itertools import chain
from pathlib import Path
import pandas as pd

from common import find_files_by_chain, update_progress, write_progress, ALLOWED_CHAINS


parser = argparse.ArgumentParser('extract meta data for cluster hits from input files')
parser.add_argument('-i', dest='inputs', nargs='+', type=Path, required=True,
                    help='a folder containing one or more DB folders')
parser.add_argument('-r', '--results', type=Path, required=True,
                    help='cluster results (hits) base folder')
parser.add_argument('-o', '--out', type=Path, required=True,
                    help='output folder path')
parser.add_argument('--summary', action='store_true', help='write cluster summary')
parser.add_argument('-c', '--chain', choices=ALLOWED_CHAINS,
                    help='chain(s) to process')
parser.add_argument('-g', '--subgroup', default='dataset_subgroup',
                    help='column to use as subgroup')
parser.add_argument('--clean', action="store_true",
                    help='remove output folder if it exists')
parser.add_argument('-d', '--retrieve-duplicates', action='store_true',
                    help='retrieve duplicated sequence entries from orginal inputs')
parser.add_argument('--web', action="store_true",
                    help='produce reduced output for web server')
parser.add_argument('--keep-singletons', action="store_true",
                    help='do not remove singleton clusters')
parser.add_argument('--progress', type=Path, help='progress file path')
args = parser.parse_args()

progress_data = {
    'totalSteps': 1,
    'currentStep': 0,
    'status': 'running',
}
write_progress(progress_data, args.progress)

logging.basicConfig(level=logging.INFO)

query_files = find_files_by_chain(args.inputs, args.chain, '*.tsv')
queries = {
    chain: pd.concat([pd.read_table(query, dtype=str) for query in queries_chain])
        for (chain, queries_chain) in query_files.items()
}

result_files = find_files_by_chain(args.results, args.chain, 'clusters.tsv')
results = {
    chain: pd.concat([pd.read_table(result, dtype=str) for result in results_chain])
        for (chain, results_chain) in result_files.items()
}

if set(queries) != set(results):
    logging.error('Query and result chains don\'t match')
    update_progress(args.progress, status='error')
    sys.exit(1)

if not queries:
    logging.error('No query data found!')
    update_progress(args.progress, status='error')
    sys.exit(1)
elif not results:
    logging.error('No result data found!')
    update_progress(args.progress, status='error')
    sys.exit(1)

if args.clean and args.out.is_dir():
    shutil.rmtree(args.out)

args.out.mkdir(parents=True)

progress_data['totalSteps'] = 2 * len(results) if args.summary else len(results)
write_progress(progress_data, args.progress)

try:
    for chain, result in results.items():
        if result.empty:
            continue

        (args.out / chain).mkdir()
        chain_out = args.out / chain / 'meta.tsv'
        query = queries[chain]

        # add duplicated entries
        if args.retrieve_duplicates:
            # this assumes sequence de-duplication was performed during prep
            # the below code may compute inconsistent cluster sizes
            query.set_index('sequence_id', inplace=True, verify_integrity=True)
            cluster_seqs = query.reindex(result['Member'])['sequence_aa']
            mask = query['sequence_aa'].isin(cluster_seqs)
            result.set_index('Member', inplace=True, verify_integrity=True)
            expanded = query.reindex(query.loc[mask].index)
            merged = result.join(expanded, how='right')
            # get rep through duplicated seq
            # some seqs are likely shared by multiple entries, just pick one of them
            seq2rep = result.join(expanded)[['sequence_aa', 'Representative']] \
                .set_index('Representative') \
                .drop_duplicates() \
                .reset_index() \
                .set_index('sequence_aa', verify_integrity=True)
            merged['Rep'] = seq2rep.reindex(merged['sequence_aa']).values
            # fix those cases where the new Rep is different from the old Rep even though it matches the sequence_id
            mismatch_mask = (merged.index == merged['Representative']) & (merged['Representative'] != merged['Rep'])
            merged.loc[mismatch_mask, 'Rep'] = merged.loc[mismatch_mask, 'Representative']
            # there are even stranger cases where sequence_id, new & old rep are all
            # different but we'll ignore them for now because these should be few

            merged['Representative'] = merged['Rep']
            merged.drop(columns='Rep').to_csv(chain_out, sep='\t')
        else:
            # assuming all members are present in the data, no deduplication done
            merged = result.merge(query, left_on='Member', right_on='sequence_id', suffixes=(None, '_'+chain))

            if len(merged) != len(result):
                logging.warning('metadata count differs from search hits for chain %s', chain)

            merged.to_csv(chain_out, sep='\t', index=None)

        update_progress(args.progress, len(merged))

        if args.summary:
            maxgroup = merged[args.subgroup].nunique()

            summary = []
            for rep, group in merged.groupby('Representative'):
                if not args.keep_singletons and len(group) == 1:
                    continue

                # work around faulty sequences with missing CDRs
                if any(group[['acdr1', 'acdr2', 'acdr3']].nunique() == 0):
                    continue

                # determine dominant CDRs and genes (they _could_ be from different entries)
                top_cdr1 = group['acdr1'].value_counts().index[0]
                top_cdr2 = group['acdr2'].value_counts().index[0]
                top_cdr3 = group['acdr3'].value_counts().index[0]
                top_vgene = group['v_call'].value_counts().index[0]
                if 'j_call' in group.columns and group['j_call'].nunique() > 0:
                    top_jgene = group['j_call'].value_counts().index[0]
                else:
                    top_jgene ='N/A'

                members = int(group['sequence_aa'].nunique())
                cluster_size = len(group)
                if args.web:
                    val = [rep, cluster_size, top_cdr1, top_cdr2, top_cdr3]
                else:
                    # val = [rep, members, cluster_size, int((group[args.subgroup] == 'yes').sum()), maxgroup] \
                    val = [rep, members, cluster_size, group[args.subgroup].nunique(), maxgroup] \
                        + [top_cdr1, top_cdr2, top_cdr3, top_vgene, top_jgene]

                summary.append(val)

            if args.web:
                with open(args.out / chain / 'summary.json', 'wt') as json_handle:
                    json.dump(summary, json_handle)
            else:
                columns = ['representative', 'members', 'cluster_size', 'donors_group', 'donors_all',
                           'cdr1', 'cdr2', 'cdr3', 'v_call', 'j_call']
                df = pd.DataFrame(summary, columns=columns).sort_values('cluster_size', ascending=False)
                df.to_csv(args.out / chain / 'summary.tsv', sep='\t', index=False)
                del df

            del summary
            update_progress(args.progress)

        del merged
        gc.collect()

except Exception:
    logging.exception('Unable to extract cluster results')
    update_progress(args.progress, status='error')
else:
    update_progress(args.progress, status='finished')
