FROM centos:7
LABEL org.sysimm.interclone.author="Systems Immunology Lab, Osaka University"
LABEL org.sysimm.interclone.description="InterClone: Store, Search and Cluster Adaptive Immune Receptor Repertoires"
LABEL org.sysimm.interclone.website="https://sysimm.org/interclone"
LABEL org.sysimm.interclone.license="GNU GPLv3"

ENV PATH /home/interclone/miniconda3/bin/:$PATH
# Update Software repository
RUN yum -y update \
    && yum install -y epel-release curl bzip2 wget gcc git \
    && yum -y clean all \
    && rm -rf /var/cache

RUN useradd interclone
WORKDIR /home/interclone/interclone/
RUN chown -R interclone:interclone /home/interclone/interclone
USER interclone
WORKDIR /home/interclone

# Install Miniconda
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh \
    && /bin/bash ~/miniconda.sh -bfp /home/interclone/miniconda3 \
    && conda update conda \
    && conda init bash \
    && rm miniconda.sh

WORKDIR /home/interclone/interclone/
COPY dependencies.yml .
RUN conda env create -f dependencies.yml && conda clean --all

COPY . .

RUN echo "conda activate interclone" >> ~/.bashrc
SHELL ["/bin/bash", "--login", "-c"]
