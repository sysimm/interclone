# Steps needed to merge hashtag reads with AIRR file data

We assume that we have a 10x tar file called
"inputs/tcr_vac_10x/27/F3301_211105_110315_cellranger.tar"

### Extract the things we need from the huge tar file

We want to extract the AIRR files and the folders containing the hashtag data
into a work folder:

`./src/dataimport/read_10x_tar.py -i inputs/tcr_vac_10x/27/F3301_211105_110315_cellranger.tar --out work/extract27/ --filter airr_rearrangement filtered_feature_bc_matrix`

My output looks like:

```
$ find work/extract27/
work/extract27/
work/extract27/F3301_211105_110315_cellranger
work/extract27/F3301_211105_110315_cellranger/27_1_5DE
work/extract27/F3301_211105_110315_cellranger/27_1_5DE/outs
work/extract27/F3301_211105_110315_cellranger/27_1_5DE/outs/filtered_feature_bc_matrix.h5
work/extract27/F3301_211105_110315_cellranger/27_1_5DE/outs/filtered_feature_bc_matrix
work/extract27/F3301_211105_110315_cellranger/27_1_5DE/outs/filtered_feature_bc_matrix/features.tsv.gz
work/extract27/F3301_211105_110315_cellranger/27_1_5DE/outs/filtered_feature_bc_matrix/barcodes.tsv.gz
work/extract27/F3301_211105_110315_cellranger/27_1_5DE/outs/filtered_feature_bc_matrix/matrix.mtx.gz
work/extract27/F3301_211105_110315_cellranger/27_2_5DE
work/extract27/F3301_211105_110315_cellranger/27_2_5DE/outs
work/extract27/F3301_211105_110315_cellranger/27_2_5DE/outs/filtered_feature_bc_matrix.h5
work/extract27/F3301_211105_110315_cellranger/27_2_5DE/outs/filtered_feature_bc_matrix
work/extract27/F3301_211105_110315_cellranger/27_2_5DE/outs/filtered_feature_bc_matrix/features.tsv.gz
work/extract27/F3301_211105_110315_cellranger/27_2_5DE/outs/filtered_feature_bc_matrix/barcodes.tsv.gz
work/extract27/F3301_211105_110315_cellranger/27_2_5DE/outs/filtered_feature_bc_matrix/matrix.mtx.gz
work/extract27/F3301_211105_110315_cellranger/27_2_TCR
work/extract27/F3301_211105_110315_cellranger/27_2_TCR/outs
work/extract27/F3301_211105_110315_cellranger/27_2_TCR/outs/airr_rearrangement.tsv
work/extract27/F3301_211105_110315_cellranger/27_1_TCR
work/extract27/F3301_211105_110315_cellranger/27_1_TCR/outs
work/extract27/F3301_211105_110315_cellranger/27_1_TCR/outs/airr_rearrangement.tsv
```

### Extract hashtags, merge with AIRR files and assign the best hashtag to a given cell barcode for all subfolders in one command:

`./src/run_extract_and_merge_hashtags.py -i work/extract27/F3301_211105_110315_cellranger -o inputs/tcr_vac_10x/27 -t TCR`

The above script makes some assumptions about folder structure but it will
check that required files exist and print an error message if they don't.
Various folder name patterns can be customized to make it work for different
inputs. In particular, there should be a unique pattern identifying the
feature folders. In the above example, that is `_5DE` and can be customized
using the `--feature-pattern` option. Secondly, the folders containing AIRR
files need to be identified. In the above example, that pattern is `_TCR` and
can be customized with the `--dataset` option. Additionally, the folder
name _outside_ of these patterns is taken as the dataset ID, i.e. `27_1` and
`27_1` in the above example. These assumptions are fairly strong and may
require a future revision.
