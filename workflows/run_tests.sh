#!/bin/sh -e

# Sample workflow to demonstrate basic usage and test the codebase. It should only take about one minute to run.
# For simplicity and convenience, this runs on local threads and does not require any queuing software like LSF.
# It also avoids the creation of database indices to save disk space. This however is not recommended for realistic
# use: the --no-index option should generally not be used.

WORKDIR=$(mktemp -d)

echo "Preparing search query..."
src/prepare_pseudo_seq.py -i inputs/woodruff166.tsv -o $WORKDIR/woodruff166 --name woodruff166 --chain H --parallel local --no-index

if [ ! -f $WORKDIR/woodruff166/H/woodruff166-mmseqs/seq.db ]
then
    echo "ERROR: dataset not found!"
    exit 1
else
    echo "OK"
fi

echo "Preparing search target..."
src/prepare_pseudo_seq.py -i inputs/database/COVID19_BCR/Woodruff-2020/Pt1ASC.tsv -o $WORKDIR/Pt1ASC --name Pt1ASC --chain H --parallel local --anarci-threads 4 --no-index

if [ ! -f $WORKDIR/Pt1ASC/H/Pt1ASC-mmseqs/seq.db ]
then
    echo "ERROR: dataset not found!"
    exit 1
else
    echo "OK"
fi

echo "Searching..."
src/search_db.py -q $WORKDIR/woodruff166 -t $WORKDIR/Pt1ASC/ -o $WORKDIR/search-woodruff --parallel local

if [ ! -f $WORKDIR/search-woodruff/H/searchhits.tsv ]
then
    echo "ERROR: result file not found!"
    exit 1
else
    echo "OK"
fi

RESULT_DIFF=$(diff -q $WORKDIR/search-woodruff/H/searchhits.tsv sample_outputs/woodruff-searchhits.tsv)
if [ -z "$RESULT_DIFF" ]
then
    echo "Success!"
else
    echo "ERROR: search results don't match"
    exit 1
fi

echo "Clustering..."
src/cluster_db.py -i $WORKDIR/Pt1ASC -o $WORKDIR/cluster-woodruff --chain H --clean

if [ ! -f $WORKDIR/cluster-woodruff/H/clusters/clusters.tsv ]
then
    echo "ERROR: cluster results not found!"
    exit 1
else
    echo "OK"
fi

diff -q $WORKDIR/cluster-woodruff/H/clusters/clusters.tsv sample_outputs/woodruff-clusters.tsv
if [ $? -eq 0 ]
then
    echo "Success!"
else
    echo "ERROR: cluster results don't match"
    exit 1
fi

rm -rf $WORKDIR
