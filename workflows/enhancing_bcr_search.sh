INPUTS_ENHAB=/u31/jwila/cdr_cluster/inputs/enhancing_bcr
INPUTS_KIM=/mounts/scratch09/zichang/200588_OPIG_OAS_masterfile_AIRR/COVID19/Kim-2021/
INPUTS_MENG=/mounts/scratch09/zichang/200588_OPIG_OAS_masterfile_AIRR/Healthy/Meng-2017/
INPUTS_GIDONI=/mounts/scratch09/zichang/200588_OPIG_OAS_masterfile_AIRR/Healthy/Gidoni-2019/
INPUTS_GHRAICHY=/mounts/scratch09/zichang/200588_OPIG_OAS_masterfile_AIRR/Healthy/Ghraichy-2020/
OUTDIR=work/benchmark/search/prep
SEARCH=work/benchmark/search/search

# prep
bsub -o prep_enhab.log ./src/prepare_pseudo_seq.py -i $INPUTS_ENHAB -o $OUTDIR/enhab --name enhancing-antibodies --chain H

bsub -o prep_kim.log ./src/prepare_pseudo_seq.py -i $INPUTS_KIM -o $OUTDIR/Kim-2021 --name COVID19_Kim-2021 --group-by-filename --chain H

bsub -o prep_meng.log ./src/prepare_pseudo_seq.py -i $INPUTS_MENG -o $OUTDIR/Meng-2017 --name Healthy_Meng-2017 --group-by-filename --chain H
bsub -o prep_gidoni.log ./src/prepare_pseudo_seq.py -i $INPUTS_GIDONI -o $OUTDIR/Gidoni-2019 --name Healthy_Gidoni-2019 --group-by-filename --chain H
bsub -o prep_ghraichy.log ./src/prepare_pseudo_seq.py -i $INPUTS_GHRAICHY -o $OUTDIR/Ghraichy-2020 --name Healthy_Ghraichy-2020 --group-by-filename --chain H

# search
bsub -o search_enhab_kim-medium.log ./src/search_db.py -q $OUTDIR/enhab -t $OUTDIR/Kim-2021/ -o $SEARCH/kim-medium --sid1 80 --sid2 80 --sid3 70 --cov 80 --parallel lsf --jobs 20
bsub -o search_enhab_ggm-medium.log ./src/search_db.py -q $OUTDIR/enhab -t $OUTDIR/[GM]* -o $SEARCH/ggm-medium --sid1 80 --sid2 80 --sid3 70 --cov 80 --parallel lsf --jobs 20

bsub -o extract_enhab_kim-medium.log ./src/extract_results.py -q $OUTDIR/enhab -t $OUTDIR/Kim-2021/ -r $SEARCH/kim-medium/ -o $SEARCH/kim-medium/meta
bsub -o extract_enhab_ggm-medium.log ./src/extract_results.py -q $OUTDIR/enhab -t $OUTDIR/[GM]* -r $SEARCH/ggm-medium/ -o $SEARCH/ggm-medium/meta

# analyze
./src/count_search_hits.py -q $OUTDIR/enhab -t $OUTDIR/Kim-2021/ -r $SEARCH/kim-medium/H/searchhits.tsv -d $SEARCH/kim-medium/hits.tsv
./src/count_search_hits.py -q $OUTDIR/enhab -t $OUTDIR/[GM]* -r $SEARCH/ggm-medium/H/searchhits.tsv -d $SEARCH/ggm-medium/hits.tsv

./src/benchmark/stacked_barplot_searchhits.py --covid $SEARCH/kim-medium/hits.tsv --healthy $SEARCH/ggm-medium/hits.tsv --out plots/enhab-search-barplot.png
