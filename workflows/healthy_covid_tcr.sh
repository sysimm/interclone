INPUTS_HEALTHY=/mounts/scratch09/zichang/200588_OPIG_OAS_masterfile_AIRR_scTCR/Healthy
INPUTS_COVID=/mounts/scratch09/zichang/200588_OPIG_OAS_masterfile_AIRR_scTCR/COVID19
HEALTHY_OUT=work/Healthy-45donors
COVID_OUT=work/COVID19-98donors

# healthy
for d in $INPUTS_HEALTHY/*
do
    dataset=Healthy_$(basename $d)
    bsub -o prep_tcr_$dataset.log ./src/prepare_pseudo_seq.py -i $d -o $HEALTHY_OUT/$dataset --name $dataset -c AB --species human --group-by-filename
done

./src/cluster_db.py -i $HEALTHY_OUT/* -o work/cluster-Healthy-cl-cov90-sid90 --cov 90 --sid 90

./src/extract_cluster_results.py -i $HEALTHY_OUT/* -r work/cluster-Healthy-cl-cov90-sid90/ -o work/cluster-Healthy-cl-cov90-sid90/meta --clean --summary

./src/view_clusters.py -i work/cluster-Healthy-cl-cov90-sid90/meta/A/summary.tsv -p -o plots/Healthy9090-clusters-log.png -log -t 'Healthy Donors'


# COVID
for d in $INPUTS_COVID/*
do
    dataset=COVID19_$(basename $d)
    bsub -o prep_tcr_$dataset.log ./src/prepare_pseudo_seq.py -i $d -o $COVID_OUT/$dataset --name $dataset -c AB --species human --group-by-filename
done

./src/cluster_db.py -i $COVID_OUT/* -o work/cluster-Covid-cl-cov90-sid90 --cov 90 --sid 90

./src/extract_cluster_results.py -i $COVID_OUT/* -r work/cluster-Covid-cl-cov90-sid90/ -o work/cluster-Covid-cl-cov90-sid90/meta --clean --summary

./src/view_clusters.py -i work/cluster-Covid-cl-cov90-sid90/meta/A/summary.tsv -p -o plots/Covid9090-clusters-log.png -log -t 'COVID19 Donors'


# combined
./src/cluster_db.py -i $HEALTHY_OUT/* $COVID_OUT/* -o work/cocluster-HealthyCovid-cl-cov90-sid90 --cov 90 --sid 90

./src/extract_cluster_results.py -i $HEALTHY_OUT/* $COVID_OUT/* -r work/cocluster-HealthyCovid-cl-cov90-sid90/ -o work/cocluster-HealthyCovid-cl-cov90-sid90/meta --clean --summary

./src/view_clusters.py -i work/cocluster-HealthyCovid-cl-cov90-sid90/meta/A/summary.tsv -p -o plots/HealthyCovid9090-clusters-log.png -log -t 'Healthy/COVID19 combined'
