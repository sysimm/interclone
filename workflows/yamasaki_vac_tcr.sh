INPUTS=/u31/jwila/cdr_cluster/inputs/tcr_vac_10x_nounk
OUTDIR=work/paper/cluster

# postvac
for dataset in 25_28 27 4_17 8_13_15
do
    bsub -o prep_yamasaki_$dataset.log ./src/prepare_pseudo_seq.py -i $INPUTS/$dataset -o $OUTDIR/postvac/prep/$dataset --name $dataset -c AB --species human --subgroup donor
done

./src/cluster_db.py -i $OUTDIR/postvac/prep/* -o $OUTDIR/postvac/cluster

./src/extract_cluster_results.py -i $OUTDIR/postvac/prep/* -r $OUTDIR/postvac/cluster/ -o $OUTDIR/postvac/meta --subgroup donor --clean --summary

./src/view_clusters.py -i $OUTDIR/postvac/meta/A/summary.tsv -p -o plots/tcr_postvac_clusters.png -t 'post vaccination'


# prevac
bsub -o prep_yamasaki_prevac.log ./src/prepare_pseudo_seq.py -i $INPUTS/prevac -o $OUTDIR/prevac/prep --name prevac -c AB --species human --subgroup donor

./src/cluster_db.py -i $OUTDIR/prevac/prep/ -o $OUTDIR/prevac/cluster

./src/extract_cluster_results.py -i $OUTDIR/prevac/prep/* -r $OUTDIR/prevac/cluster/ -o $OUTDIR/prevac/meta --subgroup donor --clean --summary

./src/view_clusters.py -i $OUTDIR/prevac/meta/A/summary.tsv -p -o plots/tcr_prevac_clusters.png -t 'pre vaccination'

./src/merge_paired_cluster_results.py -i $OUTDIR/postvac/meta -o $OUTDIR/postvac/meta/paired.tsv --member-col sequence_id
