# Workflow for DPT data

This project consists of clustering 10x BCR data that contains hashtags for
four donors whose plasmablasts were sorted using DT, PT and TT antigens
(Libra-seq) post DPT vaccination. There is a fourth antigen group (neg) which
is negative for all three antigens that probably contains plasmablasts.

## Prepare pseudo sequences

```
./src/prepare_pseudo_seq.py -i inputs/nita/DT -o public_db/nita/DT --chain HL --species human --name DT --threads 8
./src/prepare_pseudo_seq.py -i inputs/nita/PT -o public_db/nita/PT --chain HL --species human --name PT --threads 8
./src/prepare_pseudo_seq.py -i inputs/nita/TT -o public_db/nita/TT --chain HL --species human --name TT --threads 8
./src/prepare_pseudo_seq.py -i inputs/nita/neg -o public_db/nita/neg --chain HL --species human --name neg --threads 8
```

## Cluster data

```
./src/cluster_db.py -i public_db/nita/* -o public_db/nita/cluster/
```

## Extract metadata from prepared chain data and add to cluster file

```
./src/extract_cluster_results.py -i public_db/nita/* -r public_db/nita/cluster/ -o public_db/nita/cluster/meta --summary
```

## Pair the heavy- and light-chain clusters and metadata

```
./src/merge_paired_cluster_results.py -i public_db/nita/cluster/meta -o public_db/nita/cluster/meta/paired.tsv
```

## View the contents of a particular cluster (in this case, a heavy-chain cluster)

```
./src/view_clusters.py -i public_db/nita/cluster/meta/H/summary.tsv -p -o barplot.png
```
