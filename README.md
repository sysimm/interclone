# InterClone

Sequence-based tools for processing, searching, clustering and merging BCR or
TCR repertoire data.


## Getting started

### Using Docker

* get the latest image

`docker pull sysimm/interclone`

* start a shell inside the container, mounting an existing `work` folder with your data, e.g.:

`docker run -it -v "$(pwd)"/work:/home/interclone/interclone/work sysimm/interclone`

* run the tests or one of the below described workflows


### Using the code

* create the conda environment

`conda env create -f dependencies.yml`

* activate the conda environment

`conda activate cdr_cluster`

* initialize the submodules

`git submodule update --init --recursive`

* run tests

`workflows/run_tests.sh`


## Usage

There are four main functions: processing, searching, clustering and merging.
There are also several auxiliary tools to make things work easily in the real
world. The main tools are described first, followed by the auxiliary tools.


### Main Tools

#### Processing

The main workhorse for processing AIRR-formatted data is
`prepare_pseudo_seq.py`. This script takes an input folder and an output
folder as arguments.

NOTE: this code assumes existence of LSF queuing software! In environments
without LSF, you can use Slurm or local threads via the `--parallel` option
to speed up the computation. However, if you plan to use on large datasets, a
job queuing system is recommended.

To test the code, try running a very small input file:

* make a work folder 

`ln -s <some folder on a scratch disk> work`

* Process the AIRR data

`src/prepare_pseudo_seq.py -i inputs/enhancing_bcr -o work/enhancing_bcr -c H --species human --name small`

The script expects AIRR formatted TSV files with the following columns:

- sequence_id: containing a unique identifier
    - the column name can be configured using the --seq-id-column parameter
- v_call: containing the gene name, e.g. IGH or TRA
    - this column is optional; if it does not exist, no filtering will be done
      and the file is assumed to only contain data for the chain provided by
      the `-c`/`--chain` parameter
    - the column name can be configured using the --v-gene-column parameter
- sequence_aa: full length amino acid sequence
    - the column name can be configured using the --aa-seq-column parameter

The output folder is created with the same hierarchy as the input folder,
containing the output tsv files with the same names as the input files. The
output tsv files are extended by a number of columns, representing the pseudo
sequence features, which can then be used for clustering and searching.

An additional output folder that is generated contains the mmseqs
database, which can be searched or clustered using the mmseqs suite of
tools. These databases live in folders that end with "-mmseqs".


#### Searching

Once you have two or more pre-processed datasets, you can search a target
dataset another dataset as the query:

`src/search_db.py -q query_folder -t template_folder -o output_folder`

The inputs are base folders, as created by prepare_pseudo_seq. That is, they
should contain chain folders (H, L, A or B) which in turn contain mmseqs
databases. Multiple query and template folders can be supplied. Essentially,
the values given for the `-o` (output) parameter of the prep script can be
provided for the `-q` (query) and `-t` (template) parameters of the search.

The outputs will appear in `output_folder/<chain>/searchhits.tsv`. This file
contains the alignments for each hit that matches the specified similarity
thresholds. Similarity can be defined in terms of percent sequence identity
on a per-CDR basis using the flags `--sid1`, `--sid2` and `--sid3`, along
with the flag `--cov`, which defines the minimum overall coverage
cdr1+cdr2+cdr3.

With these search hits, you can extract the corresponding metadata from your
original inputs by running:

`src/extract_results.py -q query_folder -t template_folder -r output_folder -o output_meta`

Note that `query_folder` and `template_folder` should contain the input AIRR
files. `output_folder` is the result folder from the previous step. The
folder `output_meta` contains the "expanded" results which is the combination
of matched queries and templates.


#### Clustering

You can cluster a group of databse entries using mmseqs with:

`src/cluster_db.py -i query_folder -o output_folder`

The output will appear in `output_folder/<chain>/clusters/clusters.tsv`

NOTE: you can supply multiple `query_folder` values. But you should make sure
they all contain `*-pseudo_joined.fa` FASTA files from the prep pseudo
sequence generation (aka prep) step.

The output cluster file contains two columns: "Representative" and "Member".
The first column is the cluster representative. The second is the cluster
member. Both values are the sequence_id of the entry (containing the first
three parts of the string when split by `_`).

The meta data can be extracted and merged with the cluster file by:

`src/extract_cluster_results.py -i query_folder -r output_folder -o cluster_with_meta_data --summary`

The `summary` flag writes out simplified metadata associated with clusters.

To view the summary file, execute:

`src/view_clusters.py -i cluster_with_meta_data/meta/A/summary.tsv`

This will write out the contents of the clusters in order of decreasing size.
To plot the clusters as a bar plot, provide the argument `-p`. To save the
plot as a file, provide the aruments ` -o filename.ext` where ext can be any
output format supported by plotly.


#### Merging


##### Merging Search results

The above search steps are automatically run for both your heavy and light
chain data (or alpha and beta for TCRs). If you are only interested in the
common results of receptor pairs, combine the two expanded result files
with:

`src/merge_paired_results.py --heavy expanded-heavy.tsv --light expanded-light.tsv --out search_merged.tsv`

The two datasets are merged based on a common column in the input files, by
default `clone_id`. The merged result will group the entries by a new column
named `group_id`. Note that there isn't always exactly one heavy and light
chain entry because one chain of a pair could have more matches than the
other.


#### Merging Cluster results

Assuming you have prepared cluster files and extracted metadata for heavy and
light chains, you can then merge heavy and light cluster results:

`src/merge_paired_cluster_results.py --heavy cluster_H_meta.tsv --light cluster_L_meta.tsv -o cluster_HL_meta.tsv`


### auxiliary tools

#### Processing FASTA-formatted and other files

If the raw data is in FASTA format, it can be converted to AIRR using:

`src/dataimport/fasta2airr.py inputs.fa --chain IGH --output inputs.tsv`

Existing AIRR files can be validated using:

`src/dataimport/validate_airr.py inputs.tsv [ -o fixed.tsv ]`

10X CellRanger data can be converted using:

`src/dataimport/10x_cellranger.py --inputs 10x.tsv --name my_data --chain BCR --out inputs.tsv`

MiXCR output can be converted using:

`src/dataimport/mixcr.py --inputs mixcr_data_*.tsv --output inputs.tsv --name my_data`


## Support

We are happy to help if you encounter any problems that cannot be resolved
through the existing documentation. Please describe your issue on
[GitLab](https://gitlab.com/sysimm/interclone/-/issues). In case you have any
problems with using the [web server](https://sysimm.org/interclone), you can
also [contact us at via email](https://sysimm.org/contact-pages/contact-us-about-interclone).


## Further information

For more details about the employed methods as well as scientific use cases,
please refer to the paper "InterClone: Store, Search and Cluster Adaptive
Immune Receptor Repertoires" on [bioRxiv](https://www.biorxiv.org/content/10.1101/2022.07.31.501809v1).

If you use InterClone in your research, please cite it as:

Wilamowski, J., et al. InterClone: Store, Search and Cluster Adaptive Immune
Receptor Repertoires. bioRxiv 2022.07.31.501809; doi:
https://doi.org/10.1101/2022.07.31.501809
